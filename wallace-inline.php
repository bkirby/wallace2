<?php
/**
 * Plugin Name: Wallace Inline
 * Plugin URI: https://wallaceinline.com
 * Description: Front-end content editor for Beaver Builder 
 * Version: {WAL_VERSION}
 * Author: Bradley Kirby 
 * Author URI: https://wallaceinline.com
 * Copyright: (c) 2017-2018 Black Coffee Interactive LLC
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Requires PHP: 7.0
 * Text Domain: wallace-inline
 */

if (version_compare(phpversion(), '7.0.0', '<')) {
  
    add_action( 'admin_notices', function(){
        $class = 'notice notice-error';
        $message = __( 'Wallace Inline Requires PHP Version 7.0 or higher. Please upgrade your PHP version. Alternatively, contact bradley@wallaceinline.com to be provided with an older version of Wallace Inline compatible with older versions of PHP.', 'wallace-inline' );
    
        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
    } );


}
else{
    define('WAL_PLUGIN_FILE_PATH', __FILE__);
    require_once 'php/loader.php';

    

    WallaceInline\Loader::run();
}

function wal_getFieldDataFromUri($uri, $settings){
    $explode = explode(' ', $uri);


    $fieldValue = array_reduce($explode, function($acc, $uriSegment) {
        $acc = (array) $acc;
        if(array_key_exists($uriSegment, $acc)){
            return $acc[$uriSegment]; //throws notice
        }
        return '';
    }, $settings);
   
    return $fieldValue;
}

?>