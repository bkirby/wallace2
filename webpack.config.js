const path = require('path');

module.exports = {
  entry: {
    main: './js/src/index.js',
    admin: './js/src/admin.js'
  },
  devtool: 'source-map',
  devServer: {
    contentBase: './js/dist',
    disableHostCheck: true,
    // writeToDisk: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
    },

  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'js/dist'),
    publicPath: 'http://bradley.com:8080/'
  },
  mode: 'development',
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
    ]
  }
};