import React from 'react';
import Select from 'react-select';


export class InputLicense extends React.Component{
	
	constructor (props) {
		super(props);
		this.state = {
			backspaceRemoves: false,
			multi: true,
			connectionInfo: props.apiInfo,
			value: props.existingValue,
			options: props.roles
		};
		// console.log(this.state.value);

	};
	onChange = (value) => {
		// console.log(value);
		this.setState({
			value: value,
		});
		var payload = {};
		payload.user_ids = [];
		for(var user of value){
			payload.user_ids.push(user.id);
		}
		fetch(
			`${this.state.connectionInfo.walRestURL}access`,
			{
			  method: 'POST',
			  credentials: 'same-origin',  
			  headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.state.connectionInfo.postId, 'X-WP-Nonce': this.state.connectionInfo.nonce}),
			  body: JSON.stringify(payload)
			}
		)
		.then( (response) => {
			// console.log(response)
		})

	};
	gotoUser (value, event) {
		window.open(value.html_url);
	};
	
	getUsers = (input) => {
		if (!input) {
			return Promise.resolve({ options: [] });
		}

		return fetch(
		`${this.state.connectionInfo.wpRestURL}users?search=${input}`,
		{
		  method: 'GET',
		  credentials: 'same-origin',  
		  headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.state.connectionInfo.postId, 'X-WP-Nonce': this.state.connectionInfo.nonce})
		  // body: JSON.stringify(payload)
		})
		.then((response) => response.json())
		.then((users) => {
			users.forEach(function(user){
				user.name = user.name + ' (' + user.id + ')';
			});

			
			// console.log(users);
			return { options: users };
		});
	};
	
	render () {
		const AsyncComponent = Select.Async;
			

		return (
			<div className="section">
				<input type="text" id="wal-license-input" placeholder="Paste Your License Key"/>
			</div>
		);
	}
}

