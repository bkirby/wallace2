import React from 'react';
import Async from 'react-select/lib/Async';


export class SelectUsers extends React.Component{
	
	constructor (props) {
		super(props);
		this.state = {
			backspaceRemoves: false,
			multi: true,
			connectionInfo: props.apiInfo,
			value: props.existingValue
		};

	};
	onChange = (value) => {
		this.setState({
			value: value,
		});
		var payload = {};
		payload.user_ids = [];
		for(var user of value){
			payload.user_ids.push(user.id);
		}
		fetch(
			`${this.state.connectionInfo.walRestURL}access`,
			{
			  method: 'POST',
			  credentials: 'same-origin',  
			  headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.state.connectionInfo.postId, 'X-WP-Nonce': this.state.connectionInfo.nonce}),
			  body: JSON.stringify(payload)
			}
		)
		.then( (response) => {
			// console.log(response)
		})

	};
	gotoUser (value, event) {
		window.open(value.html_url);
	};
	
	getUsers = (input) => {
		if (!input) {
			return Promise.resolve({ options: [] });
		}

		return fetch(
		`${this.state.connectionInfo.wpRestURL}users?search=${input}`,
		{
		  method: 'GET',
		  credentials: 'same-origin',  
		  headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.state.connectionInfo.postId, 'X-WP-Nonce': this.state.connectionInfo.nonce})
		  // body: JSON.stringify(payload)
		})
		.then((response) => response.json())
		.then((users) => {
			users.forEach(function(user){
				user.name = user.name + ' (' + user.id + ')';
				user.label = user.name;
				user.value = user.name;


			});

			
			return users;
		});
	};
	
	render () {
			

		return (
			<div className="section">
				<Async placeholder={wallaceInline.strings.selectLabel} isMulti={true} value={this.state.value} onChange={this.onChange} onValueClick={this.gotoUser} valueKey="id" labelKey="name" loadOptions={this.getUsers} backspaceRemoves={this.state.backspaceRemoves} />
			</div>
		);
	}
}

