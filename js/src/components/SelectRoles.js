import React from 'react';
import Select from 'react-select';


export class SelectRoles extends React.Component{
	
	constructor (props) {
		super(props);
		// this.state = {
		// 	backspaceRemoves: false,
		// 	multi: true,
		// 	connectionInfo: props.apiInfo,
		// 	value: props.selectLimited ? props.roles.limited : props.roles.full,
		// 	options: props.roles.available,
		// 	selectLimited: props.selectLimited ? true : false,
		// };
		// console.log(props);
		// console.log(this.state.options);

	};
	// componentDidUpdate() {
	// 	console.log(this.state.value);
	// }
	onChange = (value) => {
		if(this.props.selectLimited){
			this.props.onLimitedRoleChange(value);
		}
		else{
			this.props.onFullRoleChange(value);
		}
		// props.
		// console.log(value);
		// this.setState({
		// 	value: value,
		// });
		// var payload = {};
		// payload.limited_role_names = [];
		// payload.full_role_names = [];
		// if(this.state.selectLimited){
		// 	for(var role of value){
		// 		payload.limited_role_names.push(role.value);
		// 	}
		// 	payload.full_role_names = this.state.existingFull;
		// }
		// else{
		// 	for(var role of value){
		// 		payload.full_role_names.push(role.value);
		// 	}
		// 	payload.limited_role_names = this.state.existingLimited;
		// }

		
		// console.log(payload);
		// fetch(
		// 	`${this.state.connectionInfo.walRestURL}access`,
		// 	{
		// 	  method: 'POST',
		// 	  credentials: 'same-origin',  
		// 	  headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.state.connectionInfo.postId, 'X-WP-Nonce': this.state.connectionInfo.nonce}),
		// 	  body: JSON.stringify(payload)
		// 	}
		// )
		// .then( (response) => {
		// 	console.log(response)
		// });

	};
	gotoUser (value, event) {
		window.open(value.html_url);
	};
	
	getUsers = (input) => {
		// if (!input) {
		// 	return Promise.resolve({ options: [] });
		// }

		// return fetch(
		// `${this.state.connectionInfo.wpRestURL}users?search=${input}`,
		// {
		//   method: 'GET',
		//   credentials: 'same-origin',  
		//   headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.state.connectionInfo.postId, 'X-WP-Nonce': this.state.connectionInfo.nonce})
		//   // body: JSON.stringify(payload)
		// })
		// .then((response) => response.json())
		// .then((users) => {
		// 	users.forEach(function(user){
		// 		user.name = user.name + ' (' + user.id + ')';
		// 	});

			
		// 	console.log(users);
		// 	return { options: users };
		// });
	};
	
	render () {
		const AsyncComponent = Select.Async;
			

		return (
			<div className="section">
				<Select
					closeOnSelect={true}
					// disabled={disabled}
					isMulti={true}
					onChange={this.onChange}
					options={this.props.roles.available}
					placeholder="Select Roles"
          			removeSelected={true}
					// rtl={this.state.rtl}
					// simpleValue
					value={this.props.selectLimited ? this.props.roles.limited : this.props.roles.full}
				/>
			</div>
		);
	}
}

