import ReactDOM from 'react-dom';
import React from 'react';

import WallaceInline from './app.js';



ReactDOM.render(
    <WallaceInline />,
     document.getElementById('wallace-inline')
   );