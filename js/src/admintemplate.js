import React from 'react';
import { Input as SemanticInput } from 'semantic-ui-react'
import styled, {css} from 'styled-components';


const AlmostStyledInput = ({licenseActive, ...props}) => <SemanticInput {...props}></SemanticInput>;
const StyledInput = styled(AlmostStyledInput)`
    button{
        height: auto;
    }
    input{
        width: 300px;
        transition: background-color 0.25s;
        background-color: white;
        ${props => props.licenseActive && css`
            background-color: hsla(115, 100%, 50%, 0.33);
        `};

    }
    &&.error{
        margin: 0;
        padding: 0;
    }
    
   
`;
const StatusMessage = styled('p')`
    font-size: 18px;
    margin: 20px 0;
`;

export class WallaceInlineAdminTemplate extends React.Component{
    constructor(props){
        super(props);
        this.state = {}
    }

    render(){
        const action = {
            onClick: this.props.submit,
            loading: this.props.loading,
            content: this.props.licenseActive ? 'Deactivate' : 'Activate'
        };

        const placeholder = this.props.licenseActive ? 'License Active' : 'Enter License Here';

        return (
            <div className="wallace-inline">
                <StyledInput onChange={(e => this.props.inputChange(e.target.value))} error={this.props.error} licenseActive={this.props.licenseActive} action={action} disabled={this.props.loading} icon='key' iconPosition='left' placeholder={placeholder}></StyledInput>
                <StatusMessage>{this.props.message}</StatusMessage>
            </div>
        )
    }

}