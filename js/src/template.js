import React from 'react';
import ReactDOM from 'react-dom';
import styled, {css} from 'styled-components';
import {SelectUsers} from './components/SelectUsers';
import {SelectRoles} from './components/SelectRoles';





const Container = styled.div`
    position: fixed;
    height: 55px;
    width: 325px;
    overflow: hidden;
    bottom: 26px;
    left: 0px;
    color: black;
    background-color: transparent;
	font-size: 12px;
	font-family: "PT Sans", system-ui, sans-serif;
    transition: ease-out transform 0.5s, opacity 0.5s;
    box-sizing: content-box;
    z-index: 9999;

    h1,h2,h3,h4,h5,h6,p,label{
        color: black;
        font-family: "PT Sans", system-ui, sans-serif;
        font-weight: 300;
    }
    
    /* Panel Open */
    transform: translateX(0%);
    opacity: 1;

    ${props => props.status === 'closed' && css`
        transform: translateX(-100%);
        opacity: 0;
    `};
    
`;

const Label = styled.label`
    padding: 4px 0 4px 4px;
    font-weight: inherit;
    width: 75px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin: 0;
    color: black;
`;

const Button = styled.button`
    color: #676F7A;
    background-color: #E4E7EA;

    width: 89px;
    font-weight: inherit;
    padding: 10px 0px;
    margin: 5px;
    border: none;
    font-family: "PT Sans", system-ui, sans-serif;
    font-weight: 300;
    border-radius: 3px;
    
    transition: background-color 0.25s, color 0.25s;
    :hover, :focus, :active{
        background-color: #dadfe5;
        color: #676F7A;
        border: none;
        position: static;
    }
    
    ${props => props.fieldChanged && css`
        background-color: #00A0D2 !important;
        color: #fff !important;
        :hover, :focus, :active{
            background-color: #0197C6 !important;
            color: #fff !important;
        }
    `};
`;

const Icon = styled.svg`
    /* display: none; */
    width: 30px;
    height: 30px;
    background-color: #E4E7EA;
    border-radius: 3px;
    margin-right: 3px;
    fill: grey;
    margin: 4px 0px 0px 1px;
    transition: background-color 0.25s, fill 0.25s;

`;


const BGButton = styled(Button)`
    display: none;
    width: 50px;
    padding: 0px;
    ${props => props.show && css`
        display: block;
    `};

`;

const LockButton = styled.button`
     width: 89px;
    font-weight: inherit;
    padding: 10px 0px;
    margin: 5px;
    border: none;
    display: none;
    width: 50px;
    padding: 0px;
    transition: background-color 0.25s;
    border-radius: 3px;

    background-color: hsla(194, 100%, 41%, 1);
    :hover, :focus{
        background-color: hsla(194, 100%, 36%, 1);
        border: none;
        svg{
            background-color: hsla(194, 100%, 36%, 1);
            fill: hsla(194, 100%, 19%, 1)
        }
    }
    svg{
        fill: hsla(194, 100%, 25%, 1);
        background-color: hsla(194, 100%, 41%, 1);
        transition: background-color 0.25s, fill 0.25s;
        width: 30px;
        height: 30px;
        border-radius: 3px;
        margin: 4px 0px 0px 1px;
    }

    ${props => props.show && css`
        display: block;
    `};
    ${props => props.locked && css`
        svg{
            fill: hsla(0, 90%, 85%, 1) !important;
            background-color: hsla(0, 90%, 50%, 1) !important;
        }
        background-color: hsla(0, 90%, 50%, 1) !important;
        :hover, :focus{
            background-color: hsla(0, 90%, 45%, 1) !important;
            svg{
	            fill: hsla(0, 90%, 80%, 1) !important;
                background-color: hsla(0, 90%, 45%, 1) !important;

            }

        }
    `};
    

`;

const DiscardButton = styled(Button)`
    margin-right: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
`;

const PublishButton = styled(Button)`
    margin-left: 0;
    margin-right: 0;

    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
`;

const EditingPanel = styled.div`
   display: flex;
   position: absolute;
   border: 2px solid #D5DADD;
   border-right: none;
   background-color: white;
   left: 0;
    height: 51px;
   
   /* top: 1px; */
   /* padding-bottom: 1px; */

   transition: ease-in transform 0.33s, opacity 0.33s;
   opacity: 1;
   z-index: 1;
   transform: scaleX(1) translateX(0);

   

    ${props => !props.fullAccess && css`
        border-right: 2px solid #D5DADD;
        border-radius: 0 5px 5px 0;
        #wal-adv-button{
            display: none;
        }
        #wal-lock-button{
            display: none;
        }
        
    `};

    ${props => props.collapse && css`
        transform: scaleX(0) translateX(0);
        opacity: 0;
    `};

    ${props => props.slide && css`
        transform: scale(1) translateX(-100%);
    `};

    #wal-adv-button{
        width: 24px;
        padding: 0px;
        background: none;
        margin-right: 5px;
        fill: black;
        transform: scale(1) translateX(0px);
        height: 100%;
        position: absolute;
        right: -28px;
        bottom: -2px;
        border: 2px solid #D5DADD;
        border-left: none;
        border-radius: 0px 25px 25px 0px;
        background-color: white;
        transition: fill 0.25s;
        use{
            transition: transform 0.25s;
        }

    };
    #wal-adv-button:hover{
	    cursor: pointer;
        fill: hsl(194, 100%, 41%);
        use{
            transform: scale(1.2) translate(-3px, -2px);
        }
        
    }
`;

const StagingPanel = styled.div`
   display: flex;
   position: absolute;
   background-color: white;
   height: 51px;
   left: 0;
   border: 2px solid #D5DADD;
   button{
    line-height: 21px;
   }
   
   /* top: 1px; */
   /* padding-bottom: 1px; */
    transition: ease-out transform 0.33s, opacity 0.33s;
    transform: scaleX(0) translateX(0);
    opacity: 0;
    z-index: 2;


    ${props => props.show && css`
        transform: scaleX(1) translateY(0);
        opacity: 1;
        z-index: 1;
    `};

   

`;

const Slider = styled.div`
    transition: ease-out transform 0.33s;
    transform: translateX(0);

    ${props => props.slide && css`
            transform: translateY(-100%);
    `};

`;

const PublishingPanel = styled.div`
    position: absolute;
    top: -2px;
    left: -2px;
    width: 650px;
    label{
        color: white;
    } 
    height: 55px;
    z-index: 1;
    display: flex;
    opacity: 1;
    background-color: hsl(194, 100%, 41%);
    transition: ease-out transform 0.33s;
    transform: translate(-50%, 100%);

    ${props => props.show && css`
        transform: translate(-50%, 0);
    `};

    ${props => props.complete && css`
        transform: translate(0%, 0);
    `};

    ${props => props.editing && css`
        opacity: 0;
    `};

`;

const PublishedNotice = styled.div`
    background-color: green;
    width: 325px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 16px;

`;

const PublishingNotice = styled.div`
    background-color: hsl(194, 100%, 41%);
    width: 325px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 16px;
    
`;

const Notice = styled.label`
    color: white;

`;

const ContextualActions = styled.div`
    position: fixed;
    width: 286px;
    bottom: 79px;
    left: 0px;
    overflow: hidden;
    color: black;
    height: 125px;
    font-size: 12px;
	font-family: "PT Sans", system-ui, sans-serif;
    font-weight: 300;
    background-color: transparent;
   transition: transform 0.66s;
    z-index: 20;
    transform: scaleY(0);
    ${props => props.show && css`
        transform: scaleY(1);
    `};

   

    h1,h2,h3,h4,h5,h6,p,label{
        color: black;
        font-family: "PT Sans", system-ui, sans-serif;
        font-weight: 300;
    }
`;


const LinkEdit = styled.div`
    display: flex;
    flex-direction: column;

   position: absolute;
   border: 2px solid #D5DADD;
   background-color: white;
   left: 0;
   bottom: 0;
   transition: transform 0.33s, opacity 0.33s;
   width: 100%;
   height: 100%;
   opacity: 0;
   transform: translateY(100%);
    ${props => props.show && css`
        transform: translateY(0%);
        opacity: 1;
    `};

    input{
        width: 90%;
        margin-left: 5%;
        margin-top: 10px;
    }
    input::placeholder{
        color: gray;
    }
    a{
        max-height: 35px;
        overflow: hidden; 
        height: 35px

    }
`;

const AdvancedSettings = styled.div`
    position: fixed;
    left: 0px;
    bottom: 80px;
    z-index: 999999;
    width: 300px;
    height: 400px;
    background-color: white;
    display: block;
    border: 2px solid #D5DADD;
    border-radius: 0 25px 25px 0 ;
    padding: 10px 10px 25px 10px;
    opacity: 0;
    color: black;
    transform: translateX(-101%);
   transition: transform 0.33s, opacity 0.33s;
   overflow-y: scroll;

    ${props => props.show && css`
        transform: translateX(0%);
        opacity: 1;
    `};
    h2{
        margin: 30px 0 40px 0;
        text-align: center;
        /* text-decoration: underline; */
        font-size: 36px;
    }
    h3{
        margin: 40px 0 10px 0;
        font-size: 24px;
        
    }

    label{
        font-style:italic;
    }


    h1,h2,h3,h4,h5,h6,p,label{
        color: black;
        font-family: "Montserrat", system-ui, sans-serif;
        font-weight: 600;

    }

    p{
        font-family: "PT Sans", system-ui, sans-serif;
        font-weight: 300;
        color: darkslategray;
        margin-bottom: 10px;
        font-size: 16px;
    }


`;




export class WallaceInlineTemplate extends React.Component{

constructor(props){
    super(props);
    this.state = {
        advancedOpen: false
    }

}


render() {
    const hasLink = this.props.linkField !== '';
    const link = hasLink ? this.props.linkField.link : '';
    const i18n = this.props.i18n;
    const iconLock = this.props.selectedField.locked ? 'closed' : 'open';

    // debugger;
    // return this.template;
    return (
        <div className="wallace-inline">
            
            <Container status={this.props.panelStatus}>
                <EditingPanel 
                    slide={['closed','publishing','published'].includes(this.props.panelStatus)} 
                    collapse={this.props.panelStatus === 'staging'}
                    fullAccess={this.props.connectionInfo.userHasFullAccess}>
                    <Label>{i18n.editorLabel}</Label>
                    <LockButton id="wal-lock-button" locked={this.props.selectedField.locked} onClick={this.props.lockClicked} show={this.props.selectedField.fieldSelector !== undefined}>
                        <svg viewBox="0 0 24 24"><use xlinkHref={"#wal-lock-"+iconLock}></use></svg>
                    </LockButton>
                    <BGButton show={this.props.canEditBg} onClick={this.props.editBg} >
                        <Icon viewBox="0 0 24 24"><use xlinkHref="#wal-bg-select"></use></Icon>
                    </BGButton>
                    <Button onClick={this.props.doneClicked} fieldChanged={this.props.fieldChanged}>{i18n.doneText}</Button>
                    <svg id='wal-adv-button' viewBox="0 0 24 24" onClick={() => this.setState({advancedOpen: !this.state.advancedOpen})}>
                        <use xlinkHref="#wal-more"></use>
                    </svg>
                    
                    
                </EditingPanel>
                
                <StagingPanel show={['staging','publishing', 'published'].includes(this.props.panelStatus)} >

                    <Slider slide={this.props.panelStatus === 'publishing'}>
                        <DiscardButton onClick={this.props.discardClicked} fieldChanged='true'>{i18n.discardText}</DiscardButton>
                        <PublishButton onClick={this.props.publishClicked} fieldChanged='true'>{i18n.publishText}</PublishButton>
                        <Button onClick={this.props.cancelClicked}>{i18n.cancelText}</Button>

                    </Slider>
                    
                    <PublishingPanel 
                        show={['publishing', 'published'].includes(this.props.panelStatus)} 
                        editing={this.props.panelStatus==='editing'} 
                        complete={this.props.panelStatus === 'published'}>

                        <PublishedNotice>
                            {/* <PublishButton onClick={this.props.cancelClicked} fieldChanged='false'>Go Back</PublishButton> */}
                            <Notice>{i18n.successStatusText}</Notice>
                        </PublishedNotice>

                        <PublishingNotice>
                            {/* <Button onClick={this.props.doneClicked} >Go Back</Button>
                            <Button onClick={this.props.completeClicked} >Complete</Button> */}
                            <Notice>{i18n.publishingStatusText}</Notice>

                        </PublishingNotice>
                    </PublishingPanel> 
                </StagingPanel>
            </Container>

            <ContextualActions show={(this.props.linkField !== '' || this.props.externalSelected) && this.props.panelStatus === 'editing' }>

                <p show={this.props.externalSelected ? 'true' : 'false'}>EXTERNAL</p>
                <LinkEdit show={this.props.linkField !== '' && this.props.panelStatus === 'editing'}>
                        <label>{i18n.editLink}</label>
                        <a href={link} target="_blank">{link}</a>
                        <input type='url' value='' placeholder={i18n.pasteLink} onPaste={(e) => this.props.linkChange(e, this.props.linkField)}>
                        </input>
                </LinkEdit>

            </ContextualActions>

            <AdvancedSettings show={this.props.panelStatus === 'editing' && this.state.advancedOpen}>
                <div>
                    <label id="wal-settings-menu-label">{i18n.advancedSettings}</label>
                    <h2>{i18n.roleLabel}</h2>
                    <hr/>
                    <h3>{i18n.fullCapabilityLabel}</h3>
                    <p>{i18n.fullCapabilityDescription}</p>
                    <SelectRoles roles={ this.props.uac } selectLimited={false} onFullRoleChange={this.props.onFullRoleChange} apiInfo={ this.props.connectionInfo } />

                    <h3>{i18n.limitedCapabilityLabel}</h3>
                    <p>{i18n.limitedCapabilityDescription}</p>
                    <SelectRoles roles={ this.props.uac } selectLimited={true} onLimitedRoleChange={this.props.onLimitedRoleChange}  apiInfo={ this.props.connectionInfo } />
                    
                    <h3>{i18n.singlePageLabel}</h3>
                    <p>{i18n.singlePageDescription}</p>
                    <SelectUsers apiInfo={ this.props.connectionInfo } existingValue={ this.props.uac.overrides }/>
                </div>
            </AdvancedSettings>
        </div>

    );
}
}


