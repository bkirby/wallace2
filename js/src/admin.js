import {from, fromEvent, Observable } from 'rxjs';
import { map, scan, filter, toArray} from 'rxjs/operators';
import { Input } from 'semantic-ui-react'
import ReactDOM from 'react-dom';
import React from 'react';
import {hot} from 'react-hot-loader/root';
import {WallaceInlineAdminTemplate} from './admintemplate.js';


class WallaceInlineAdmin extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            licenseActive: wallaceInlineAdminInit.licenseActive,
            loading: false,
            error: false,
        };
        this.connectionInfo = wallaceInlineAdminInit.connectionInfo;
        this.licenseInput = '';
        this.message = wallaceInlineAdminInit.licenseActive === 'true' ? 'License Active! Thanks for being a Wallace Inline customer!': '';
    }
    inputChange = (newValue) => {
        this.licenseInput = newValue;
    }

    submit = () => {
        this.setState({
            loading: true
        });
        fetch(this.connectionInfo.walRestURL + 'license', {
            method: 'POST',
            credentials: 'same-origin',  
            headers: new Headers({'content-type': 'application/json', 'X-WP-Nonce': this.connectionInfo.nonce}),
            body: JSON.stringify({
                license: this.licenseInput,
                edd_action: this.state.licenseActive ? 'deactivate_license' : 'activate_license',
                item_name: this.connectionInfo.itemName,
                url: this.connectionInfo.siteURL,
                networkAdmin: this.connectionInfo.networkAdmin
            })
        }).then( response => {
            response.json().then(result => {
                this.message = result.message;
                this.setState({
                    loading: false,
                    licenseActive: result.success ? !this.state.licenseActive : this.state.licenseActive,
                    error: !result.success,
                });
            });
        });

       
    }

    render(){
        return <WallaceInlineAdminTemplate 
            {...this.state}
            submit={this.submit}
            inputChange={this.inputChange}
            message={this.message}
        />;
    }


}


export default hot(WallaceInlineAdmin);

ReactDOM.render(
    <WallaceInlineAdmin/>,
     document.getElementById('wal-license-app')
);
