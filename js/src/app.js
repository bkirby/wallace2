import {from, fromEvent, Observable } from 'rxjs';
import { map, scan, filter, toArray} from 'rxjs/operators';

import React from 'react';
import {hot} from 'react-hot-loader/root';


import * as tinymce from 'tinymce/tinymce'; //???
import 'tinymce/themes/silver';

import 'tinymce/plugins/quickbars';
import 'tinymce/plugins/link';
import 'tinymce/plugins/paste';


import {WallaceInlineTemplate} from './template.js';


class WallaceInline extends React.Component {

constructor(props){
    super(props);
    this.connectionInfo = {};
    this.state = {
        panelStatus: 'closed', //closed|editing|staging|publishing|published
        panelDrawerOpen: false,
        selectedField: {},
        fieldChanged: false,
        editableFields: [],
        uac : {
            available: wallaceInline.uac.roles,
            limited: wallaceInline.uac.limitedRoles,
            full: wallaceInline.uac.fullRoles,
            overrides: wallaceInline.uac.userOverrides
          }
    }

    // console.log(wallaceInline.editableFields);

    this.editableFields$ = from(wallaceInline.editableFields).pipe(
        map(field => this.addElemRef(field)),
        filter(field => field.elemRef !== null && field.elemRef !== undefined)
    );
    
    this.changedFields = {};
    this.changedFields$ = fromEvent(document.getElementById('wallace-inline'), 'fieldChanged').pipe(
        map((e) => {
            return e.detail
        }),
        scan((acc, fieldData) => {
            return {...acc, [fieldData.field.fieldSelector]: fieldData};
        }, (fieldData) => {return {[fieldData.field.fieldSelector]: fieldData}})
    );

    this.connectionInfo = wallaceInline.connectionInfo;
    this.translatedStrings = wallaceInline.strings;

}

componentDidMount() {

    if(wallaceInline.editableFields.length < 1){
        document.getElementById('wp-admin-bar-wal-inline-frontend-edit-link').style.display = 'none';
    }

    document.addEventListener('walInlineAdminButtonClicked', (e) => {
        this.setState((prevState) => ({
            panelStatus: prevState.panelStatus === 'closed' ? 'editing': prevState.panelStatus
        }));
    });

    this.changedFields$.subscribe((fields) => {
        this.changedFields = fields;
        if(!this.state.fieldChanged){
            this.setState({fieldChanged: true});
        }
    });

    this.editableFields$.pipe(toArray()).subscribe(fields => {
        this.setState({editableFields: fields});
    });

    if(wallaceInline.openImmediately){
        history.pushState({}, document.title, window.location.pathname);
        this.setState({panelStatus: 'editing'});

    }

    window.onbeforeunload = (e) => {
        if(this.state.fieldChanged){
            return 'Changes may not be saved.';
        }
    }



}

componentDidUpdate(prevProps, prevState){
    if(prevState.panelStatus === 'closed' && this.state.panelStatus !== 'closed'){ //controls opened
        this.editableFields$.subscribe(field => {
           this.activateField(field);
        });
        document.dispatchEvent(new CustomEvent("WallaceInlineOpened"));
    }
    if(prevState.panelStatus !== 'closed' && this.state.panelStatus === 'closed'){ //controls closed
        this.editableFields$.subscribe(field => {
            // debugger;
           this.deactivateField(field);
           if(field.elemRef){
                // tinymce.execCommand('mceRemoveControl', true, '');
                delete tinyMCE.editors[field.elemRef.getAttribute('id')];

           }
        });
        // tinymce.remove();
        // tinymce.get('.wal-selectable').remove();
        document.dispatchEvent(new CustomEvent("WallaceInlineClosed"));

    }
    if(prevState.selectedField.fieldSelector !== this.state.selectedField.fieldSelector){
        if(prevState.selectedField.elemRef){
            prevState.selectedField.elemRef.classList.remove('wal-selected'); 
        }
        if(this.state.selectedField.elemRef){
            this.state.selectedField.elemRef.classList.add('wal-selected'); 
        } 
    }
    if(prevState.panelStatus !== 'published' && this.state.panelStatus === 'published'){
        setTimeout(() => {
            this.setState({panelStatus: 'editing'});
            this.setState({fieldChanged: false});
            this.changedFields = {};
        }, 1000);
    }

}

//stat1 dyn1 stat2
//elem: stat1 [dyn1] uri:stat2
//elem stat1 [dyn1] stat2

addElemRef = (field) => {
    
    const dynamicTest = / \{(\d+)\}/;

    const dynamicMatch = field.fieldSelector.match(dynamicTest);

    const fromDynamic = (field) => {

        const instanceId = dynamicTest.exec(field.fieldSelector)[1];

        const subSelectorTest = /.+? ?\{\d+\}/;
        const subSelectorSegment = subSelectorTest.exec(field.fieldSelector)[0];
        const remainder = field.fieldSelector.replace(subSelectorSegment, '');


        const fromDynamic2 = (elem2, selector2) => {
            const instanceId2 = dynamicTest.exec(selector2)[1];
            
            const subSelectorSegment2 = subSelectorTest.exec(selector2)[0];
            const remainder2 = selector2.replace(subSelectorSegment2, '');
            
            if(remainder2 !== ''){
                selector2 = selector2.replace(dynamicTest, '');
                elem2 = elem2.querySelectorAll(selector2)[instanceId2];
                field.elemRef = elem2;
                return field;
            }
            selector2 = selector2.replace(dynamicTest, '');
            elem2 = elem2.querySelectorAll(selector2)[instanceId2];
            field.elemRef = elem2;
            
            return field;

        }

        
        
        if(remainder !== ''){
            
            const selector = subSelectorSegment.replace(dynamicTest, '');
            const subElem = document.querySelectorAll(selector)[instanceId];
            if(remainder.match(dynamicTest)){
                field = fromDynamic2(subElem, remainder);
                return field;

            }
            field.elemRef = subElem.querySelector(remainder);
            return field;
        }

        const selector = field.fieldSelector.replace(dynamicTest, '');
        const elem = document.querySelectorAll(selector)[instanceId];
        field.elemRef = elem;
        
        return field;

        
    }
    if(dynamicMatch){
        field = fromDynamic(field);
    }
    else{
        const elem = document.querySelector(field.fieldSelector);
        field.elemRef = elem;
    }

    if(field.elemRef){
        if(field.uiType === 'media'){
            //figure out what to do about media having two relevant elems (usually <img> and its parent)
            field.elemRef.classList.add('wal-media');
            var editLabel = field.elemRef.querySelector('.wal-media-edit-button');
            if(!editLabel){
                var editLabel = document.createElement('div');
                editLabel.innerHTML = 'Edit';
                editLabel.classList.add('wal-media-edit-button');
                field.elemRef.prepend(editLabel);
            }
           
        }
        field.elemRef.classList.add('wal-field');
    }
    return field;
}

elemRefFromSelector = (selector) => {
    const dynamicTest = / \{(\d+)\}/;

    const dynamicMatch = selector.match(dynamicTest);

    const fromDynamic = (selector) => {

        const instanceId = dynamicTest.exec(selector)[1];

        const subSelectorTest = /.+? ?\{\d+\}/;
        const subSelectorSegment = subSelectorTest.exec(selector)[0];
        const remainder = selector.replace(subSelectorSegment, '');

        if(remainder !== ''){
            const selector = subSelectorSegment.replace(dynamicTest, '');
            const subElem = document.querySelectorAll(selector)[instanceId];
            return subElem.querySelector(remainder);
        }

        const processedSelector = selector.replace(dynamicTest, '');
        const elem = document.querySelectorAll(processedSelector)[instanceId];
        return elem;
        
    }
    if(dynamicMatch){
        return fromDynamic(selector);
    }
    else{
        const elem = document.querySelector(selector);
        return elem;
    }

    
}

detectBgMedia = (field) => {


}

panelOpen = () => {
    return !this.state.panelStatus === 'closed';
}

emitFieldChangeEvent = (changeObject) => {


    if(this.state.panelStatus !== 'closed'){
        document.getElementById('wallace-inline')
        .dispatchEvent(new CustomEvent('fieldChanged', 
            {detail: changeObject}
        ));
    }
    
}

globalFieldClickListener = (e) => e.preventDefault(); //prevent other click events, including links


activateField = (field) => {
    if(field.elemRef){
        field.elemRef.addEventListener('click', this.globalFieldClickListener);
        if(!['bgMedia', 'link'].includes(field.uiType)){
            field.elemRef.classList.add('wal-selectable');
        }
        if(field.locked){
            field.elemRef.classList.add('wal-lock');
        }
        if(field.dataSource.external){
            field.elemRef.classList.add('wal-external');
        }
    }
    switch(field.uiType){
        case 'text':
            setTimeout(() => {
                tinymce.init({
                    target: field.elemRef,
                    skin: false, //load this manually
                    content_css: false,
                    browser_spellcheck: true,
                    plugins: ['quickbars', 'link', 'paste'],
                    paste_as_text: true,
                    // theme: 'silver',
                    quickbars_selection_toolbar: ['heading', 'button'].includes(field.moduleSlug) ? 
                        'undo redo | bold italic underline link':
                        'undo redo | bold italic underline alignleft aligncenter alignright link',
                    toolbar: false,
                    menubar: false,
                    inline: true,
                    verify_html: false,
                    init_instance_callback: ((field, thisReactComponent) => {
    
                        return (editor) => {
                            editor.on('click', () => {
                                thisReactComponent.setState({selectedField: field});
                            });
                            // editor.on('input', () => {
                            //     thisReactComponent.emitFieldChangeEvent({
                            //         field: field, data: editor.getContent({format: 'raw'})
                            //     });

                            const appendBogus = () => { //prevents empty elements from being deleted and not recognized by wallace inline
                                let el = editor.getElement()
                                if ('SPAN' === el.tagName && ! el.querySelector('br[data-mce-bogus]')) {
                                  let bogus = document.createElement('br')
                                  bogus.setAttribute('data-mce-bogus', '1')
                                  el.append(bogus)
                                }
                              }
                            
                            //   if(field.moduleSlug !== 'list'){
                            //     appendBogus();
                            //     editor.on('keydown', appendBogus );
                            //   }
                              
                            // });
                            editor.on('Paste Change input Undo Redo', () => {
                                // console.log(field);
                                thisReactComponent.emitFieldChangeEvent({
                                    field: field, data: editor.getContent({format: 'raw'})
                                });
                            });
                        }
                    })(field, this)
                });
            }, 500);
            break;
        case 'media':
            const editButton = field.elemRef.querySelector('.wal-media-edit-button');
            const imgElem = field.elemRef.querySelector('img');

            const openMediaSelector = (e) => {

                e.target.removeEventListener('click', openMediaSelector);

                var frame = wp.media({
                    title: 'Select Media',
                    button: {
                      text: 'Select'
                    },
                    multiple: false  // Set to true to allow multiple files to be selected
                  });
                frame.on('select', () => {
                    var attachment = frame.state().get('selection').first().toJSON();
                    imgElem.src = attachment.sizes['full'].url;
                    imgElem.srcset = '';
                    imgElem.removeAttribute('width');
                    imgElem.removeAttribute('height');
                    this.emitFieldChangeEvent({
                        field: field, data: attachment.id
                    });
                    this.setState({selectedField: {}});
                });
                
                frame.open();
            }

            const mediaListener = (e) => {

                this.setState({selectedField: field});
                editButton.addEventListener('click', openMediaSelector);
            }
            field.listener = mediaListener;
            // debugger;
            field.elemRef.addEventListener('click', field.listener);
            break;
        case 'icon':
            const openIconSelector = (e) => {
                this.setState({selectedField: field});

			    const selector = jQuery( '<div class="fl-lightbox-wrap fl-icon-selector"><div class="fl-lightbox-mask"></div><div class="fl-lightbox"><div class="fl-lightbox-content-wrap"><div class="fl-lightbox-content"></div></div></div></div>' );
			    jQuery( 'body' ).prepend( selector );
                selector.find('.fl-lightbox-content').html(wallaceInline.iconSelectorHTML.html);


			    jQuery('.fl-icons-list i').on('click', (e) => {
                    const icon = jQuery(e.currentTarget).attr('class');

                    this.emitFieldChangeEvent({
                        field: field, data: icon
                    });
                    if(field.moduleSlug === 'list-icon'){
                        const icons = Array.from(document.querySelectorAll('.fl-node-'+field.moduleId+' i'))
                        for(var i=0; i<icons.length; i++){
                            const iconElem = icons[i];
                            const currentIcon = field.moduleSettings.icon;
                            iconElem.className = iconElem.className.replace(currentIcon, icon);
                        }
                        field.moduleSettings.icon = icon;
                    }
                    else if(field.moduleSlug === 'pp-iconlist'){
                        const icons = Array.from(document.querySelectorAll('.fl-node-'+field.moduleId+' .pp-list-item-icon'));
                        for(var i=0; i<icons.length; i++){
                            const iconElem = icons[i];
                            const currentIcon = field.moduleSettings.list_icon;
                            iconElem.className = iconElem.className.replace(currentIcon, icon);
                        }
                        field.moduleSettings.icon = icon;
                    }
                    else{
                        field.elemRef.className = field.elemRef.className.replace(field.icon, icon);
                        field.icon = icon;
                    }
                    selector.remove();
                });

                
                jQuery('.fl-icons-filter-select').on('change', () => {
			        const section = jQuery( '.fl-icons-filter-select' ).val();
                    if ( 'all' == section ) {
                        jQuery( '.fl-icons-section' ).show();
                    }
                    else {
                        jQuery( '.fl-icons-section' ).hide();
                        jQuery( '.fl-' + section ).show();
                    }
                });

                jQuery('.fl-icons-filter-text').on('keyup', () => {
                    const search = jQuery( '.fl-icons-filter-text' ).val();
                    if ( '' !== search ) {
                        jQuery( '.fl-icons-list i' ).each( (index, icon) => {
                            if ( -1 == jQuery(icon).attr( 'class' ).indexOf( search ) ) {
                                jQuery(icon).hide();
                            }
                            else {
                                jQuery(icon).show();
                            }
                        });
                    }
                    else {
                        jQuery( '.fl-icons-list i' ).show();
                    }
                });
                
                jQuery('.fl-icon-selector-cancel').on('click', () => {
                    selector.remove();

                });

            }
            field.listener = openIconSelector;
            
            if(field.altSelect){
                const elemRef = this.elemRefFromSelector(field.altSelect);
                elemRef.addEventListener('click', openIconSelector);
            }
            else{
                field.elemRef.addEventListener('click', openIconSelector);
            }
            break;
        case 'link':
            if(field.fieldSelector.includes('.uabb-info-list-link')){
                field.elemRef.style.zIndex = -1;
            }
            if(field.fieldSelector.includes('.uabb-infobox-module-link')){
                field.elemRef.style.zIndex = -1;
            }

   }
}

deactivateField = (field) => {
    if(field.elemRef){
        field.elemRef.classList.remove('wal-selectable');
        field.elemRef.removeEventListener('click', this.globalFieldClickListener);
    }
    if(field.altSelect){
        const elemRef = this.elemRefFromSelector(field.altSelect);
        elemRef.removeEventListener('click', field.listener);
    }
    if(field.listener){
        field.elemRef.removeEventListener('click', field.listener);
    }
    if(field.fieldSelector.includes('.uabb-info-list-link')){
        field.elemRef.style.zIndex = 9;
    }
    if(field.fieldSelector.includes('.uabb-infobox-module-link')){
        field.elemRef.style.zIndex = 4;
    }
}

doneClicked = () => {
    if(this.state.fieldChanged){
        this.setState({panelStatus: 'staging'});
        return;
    }
    this.setState({panelStatus: 'closed'});
}
discardClicked = () => {
    location.reload();
}

publishClicked = () => {
   for (var key in this.changedFields){
       if(this.changedFields[key].field.wrapper){
            const prepend = this.changedFields[key].field.wrapper[0];
            const append = this.changedFields[key].field.wrapper[1];
            this.changedFields[key].data = prepend + this.changedFields[key].data + append;
       }
   }
    this.setState({panelStatus: 'publishing'});
    // debugger;
    console.log(this.changedFields);
    fetch(this.connectionInfo.walRestURL+'publish', {
        method: 'POST',
        credentials: 'same-origin',  
        headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.connectionInfo.postId, 'X-WP-Nonce': this.connectionInfo.nonce}),
        body: JSON.stringify(this.changedFields)
    }).then( response => this.setState({panelStatus: 'published', selectedField: {}}));

}

cancelClicked = () => {
    this.setState({panelStatus: 'editing'});
}

completeClicked = () => {
    this.setState({panelStatus: 'published'});
}

canEditBg = () => {
    const match = this.state.editableFields.filter(field => {
        if(field.moduleId === this.state.selectedField.rowId){
            return true;
        }
        return false;
    });
    if(match.length < 1){
        return false;
    }
    return true;
}

editBg = () => {
    const bgField = this.state.editableFields.filter(field => {
        if(field.moduleId === this.state.selectedField.rowId){
            return true;
        }
        return false;
    })[0];

    var frame = wp.media({
        title: 'Select Media',
        button: {
          text: 'Select'
        },
        multiple: false  
      });
    frame.on('select', () => {
        var attachment = frame.state().get('selection').first().toJSON();
        bgField.elemRef.style.backgroundImage = 'url(' + attachment.sizes['full'].url + ')';
        this.emitFieldChangeEvent({
            field: bgField, data: attachment.id
        });
    });
    frame.open();
}

lockClicked = () => {
    if(!this.state.selectedField.locked){
        this.state.selectedField.elemRef.classList.add('wal-lock');
    }
    if(this.state.selectedField.locked){
        this.state.selectedField.elemRef.classList.remove('wal-lock');
    }
    
    var payload = {module_id: this.state.selectedField.moduleId, uri: this.state.selectedField.fieldUri, locked: !this.state.selectedField.locked};
    fetch(
        this.connectionInfo.walRestURL+'lock', 
        {
          method: 'POST',
          credentials: 'same-origin',  
          headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.connectionInfo.postId, 'X-WP-Nonce': this.connectionInfo.nonce}),
          body: JSON.stringify(payload)
        }).then( (response) => {
        
        });

    this.setState({
        selectedField: {...this.state.selectedField, locked: !this.state.selectedField.locked},
        editableFields: this.state.editableFields.map((field) => {
            if(field.fieldSelector === this.state.selectedField.fieldSelector){
                field.locked = !field.locked;
            }
            return field;
        }) 
    });

}

externalSelected = () => {
    // console.log(this.state.selectedField.dataSource.external);
    if(this.state.selectedField.dataSource === undefined){
        return false;
    }
    return this.state.selectedField.dataSource.external;
}

linkSelected = () => {
    if(this.state.selectedField.fieldSelector === undefined){
        return '';
    }
    const match = this.state.editableFields.filter(field => {

        if(field.context){
            if(field.context.includes(this.state.selectedField.fieldSelector)){
                return true;
            }
        } 
        return false;
    });
    if(match.length < 1){
        return '';
    }
    console.log(match[0]);
    return match[0];
}



linkChanged = (e, linkField) => {

    const newLink = e.clipboardData.getData('text');

    this.setState({
        editableFields: this.state.editableFields.map(field => {
            if(field.fieldSelector === linkField.fieldSelector){
                field.link = newLink;
                return field;
            }
            return field;
        })
    });
    this.emitFieldChangeEvent({
        field: linkField, data: newLink
    });

    linkField.elemRef.setAttribute('href', newLink); 
    

}

onFullRoleChange = (fullRoles) => {
    this.setState((prevState, props) => ({
      uac: {
        available: prevState.uac.available,
        limited: prevState.uac.limited.filter(role => fullRoles.find(fullRole => fullRole.value === role.value) === undefined),
        full: fullRoles,
        overrides: prevState.uac.overrides

      }
    }), () => {this.initRoleChangeRequest(this.state.roles)});
  
  };
  
    onLimitedRoleChange = (limitedRoles) => {
      this.setState((prevState, props) => ({
        uac: {
          available: prevState.uac.available,
          limited: limitedRoles,
          full: prevState.uac.full.filter(role => limitedRoles.find(limRole => limRole.value === role.value) === undefined ),
          overrides: prevState.uac.overrides
        }
      }), () => {this.initRoleChangeRequest(this.state.roles)});
  
    };

    initRoleChangeRequest = (roles) => {

   
        var payload = {};
        payload.limited_role_names = this.state.uac.limited.map(role => role.value);
        payload.full_role_names = this.state.uac.full.map(role => role.value);
        fetch(
         `${this.connectionInfo.walRestURL}access`,
         {
           method: 'POST',
           credentials: 'same-origin',  
           headers: new Headers({'content-type': 'application/json', 'X-Post-Id': this.connectionInfo.postId, 'X-WP-Nonce': this.connectionInfo.nonce}),
           body: JSON.stringify(payload)
         }
        )
        .then( (response) => {
         // console.log(response)
        });
      }
  




render(){
    return <WallaceInlineTemplate {...this.state}
        doneClicked={this.doneClicked}
        discardClicked={this.discardClicked}
        publishClicked={this.publishClicked}
        cancelClicked={this.cancelClicked}
        completeClicked={this.completeClicked}
        externalSelected={this.externalSelected()}
        canEditBg={this.canEditBg()}
        editBg={this.editBg}
        linkChange={this.linkChanged}
        linkField={this.linkSelected()}
        connectionInfo={wallaceInline.connectionInfo}
        onFullRoleChange={this.onFullRoleChange}
        onLimitedRoleChange={this.onLimitedRoleChange}
        i18n={this.translatedStrings}
        lockClicked={this.lockClicked}
    />;
}

}

export default hot(WallaceInline);




