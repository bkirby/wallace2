��    &      L  5   |      P     Q     b  )   }     �     �     �     �     �     �     �     �            ?   *     j     w     �     �     �     �     �     �  .   �     	          &  	   =     G     P     o  b   x  L   �     (     7  �   F  x   �  4   @    u     �
     �
  3   �
                     /     7     >     F     [     p     u  I   �     �     �  
   �     �               '     0  9   >     x     �     �     �     �  !   �     �  h   �  K   X     �     �  �   �  �   g  B   �                                               
         $                                  "   	           &             %                      !   #                                    Activate License Activate the Saved License Allow editing by Limited Capability users Cancel Change background image Deactivate License Discard Done Edit Enter License Key Enter your license key Error Full Capability Give individual users Limited Capability to edit only this page Instructions License Active License Key License Management Limited Capability Live Editor Publish Publishing... Restrict editing to Full Capability users only Role Management Save Changes Select or Upload Media Select... Settings Single-Page Limited Capability Success! There is a new version of %1$s available. %2$sView version %3$s details%4$s or %5$supdate now%6$s. There is a new version of %1$s available. %2$sView version %3$s details%4$s. Type to search Use this image Users with these roles have full plugin capabilites including managing other users' permissions. Typically these are site admins Users with these roles have their editing access limited by Full Capability users and can't edit other users' permission You do not have permission to install plugin updates Project-Id-Version: wallace-inline
Report-Msgid-Bugs-To: Translator Name <translations@example.com>
POT-Creation-Date: 2018-07-27 15:55-0400
PO-Revision-Date: 
Last-Translator: 
Language-Team: Bradley Kirby <bradley@wallaceinline.com>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Textdomain-Support: yesX-Generator: Poedit 1.6.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html_x:1,2c;esc_html__;esc_attr_e;esc_attr_x:1,2c;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c;_n:1,2;_n_noop:1,2;__ngettext:1,2;__ngettext_noop:1,2;_c,_nc:4c,1,2
X-Poedit-Basepath: ..
X-Generator: Poedit 2.0.6
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: node_modules
 Aktiver den lagrede lisensen Aktiver den lagrede lisensen Tillat redigering for brukere med begrenset tilgang Avbryt Endre bakgrunnsbilde Deaktiver lisensen Forkast Ferdig Rediger Skriv inn lisenskode Skriv inn lisenskode Feil Full tilgang Gi individuelle brukere begrenset tilgang til å redigere kun denne siden Bruksanvisning Lisens Aktiv Lisenskode Lisenshåndtering Begrenset tilgang Live redigering Publiser Publiserer… Begrens redigering til å gjelde brukere med full tilgang Rollestyring Lagre endringer Velg eller last opp media Velg… Instillinger Begrenset tilgang til enkeltsider Suksess! En ny versjon av %1$s er tilgjengelig. %2$s Vis versjon %3$s detaljer %4$s eller %5$s oppdater nå %6$s. En ny versjon av %1$s er tilgjengelig. %2$s Vis versjon %3$s detaljer %4$s. Skriv for å søke Bruk dette bildet Brukere med disse rollene har full tilgang til utvidelsen, inkludert styring av tilgangen til andre brukere. Disse er vanligvis administratorer av nettstedet Brukere med disse rollene har fått begrenset redigeringsmuligheter av brukere med full tilgang og kan heller ikke endre tilgang for andre brukere Du har ikke tillatelse til å installere oppdateringer for tillegg 