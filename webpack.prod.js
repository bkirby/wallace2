const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    main: './js/src/index.js',
    admin: './js/src/admin.js'
  },
  devtool: 'cheap-module-source-map',
 
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'js/dist'),
  },
  plugins: [
    
   ],
mode: 'production',
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
    ]
  }
};