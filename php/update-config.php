<?php




//update and license check for edd

// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'WAL_STORE_URL', 'https://wallaceinline.com' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'WAL_ITEM_NAME', getenv('WAL_ITEM_NAME') ); // you should use your own CONSTANT name, and be sure to replace it throughout this file
// define( 'WAL_ITEM_NAME', {'Wallace Inline}' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of the settings page for the license input to be displayed
define( 'WAL_LICENSE_PAGE', 'wallaceinline-license' );

if( !class_exists( 'Wal_Plugin_Updater' ) ) {
	// load our custom updater
	include( 'Wal_Plugin_Updater.php' );
}

function wal_plugin_updater() {

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'wal_license_key' ) );
	if(get_site_option('wal_license_key') !== false){
		$license_key = trim( get_site_option( 'wal_license_key' ) );
	}

	// setup the updater
	$edd_updater = new Wal_Plugin_Updater( WAL_STORE_URL, WAL_PLUGIN_FILE_PATH, array(
			'version' 	=> getenv('WAL_VERSION'), 				// current version number
			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
			'item_name' => WAL_ITEM_NAME, 	// name of this plugin
			'item_id' => '{WAL_PRODUCT_ID}',				//id of this plugin
			'author' 	=> 'Bradley Kirby',  // author of this plugin
			'beta'		=> false
		)
	);

		// Traditional WordPress plugin locale filter
				// Uses get_user_locale() which was added in 4.7 so we need to check its available.
		if ( function_exists( 'get_user_locale' ) ) {
			$locale = apply_filters( 'plugin_locale', get_user_locale(), 'fl-builder' );
		} else {
			$locale = apply_filters( 'plugin_locale', get_locale(), 'fl-builder' );
		}


		//Setup paths to current locale file
		$mofile_global = trailingslashit( WP_LANG_DIR ) . 'plugins/wallace-inline-' . $locale . '.mo';
		$mofile_local  = trailingslashit( dirname(WAL_PLUGIN_FILE_PATH) ) . 'languages/' . $locale . '.mo';


		if ( file_exists( $mofile_global ) ) {
			//Look in global /wp-content/languages/plugins/ folder
			return load_textdomain( 'wallace-inline', $mofile_global );
		} elseif ( file_exists( $mofile_local ) ) {
			//Look in local /wp-content/plugins/wallace-inline/languages/ folder
			return load_textdomain( 'wallace-inline', $mofile_local );
		}

		//Nothing found
		return false;


}
add_action( 'admin_init', 'wal_plugin_updater', 0 );

/************************************
* OPTIONS PAGE
*************************************/

function wal_license_menu() {
	if(!is_multisite()){
		add_options_page( 'Wallace Inline', 'Wallace Inline', 'manage_options', WAL_LICENSE_PAGE, 'wal_license_page' );
	}
}
add_action('admin_menu', 'wal_license_menu');

function wal_super_license_menu() {
	add_submenu_page( 'settings.php', 'Wallace Inline', 'Wallace Inline',
    'manage_network_options', 'wallaceinline-network-license', 'wal_license_page');
}
add_action('network_admin_menu', 'wal_super_license_menu');

function wal_license_script($pageName){
	if($pageName === "settings_page_wallaceinline-network-license" ||
		$pageName === "settings_page_wallaceinline-license"){
		// wp_enqueue_script( 'wallace-inline', plugin_dir_url( __FILE__ ) . '../js/dist/admin.js', array('tinymce_js'), '1.0', true);
		wp_enqueue_style('semantic-ui-default-theme', 'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css', array(), '1.0', false);
		wp_enqueue_script( 'wallace-inline', 'http://localhost:8080/admin.js', array(), '1.0', true);
	}
}
add_action('admin_enqueue_scripts', 'wal_license_script');

function wal_license_page(){

	if(is_multisite() && get_site_option('wal_license_key') === false && get_option('wal_license_key') !== false){
		update_site_option('wal_license_key', get_option('wal_license_key'));
	}

	
	$currentLicense = trim( get_option( 'wal_license_key' ) );
	if(get_site_option('wal_license_key') !== false){
		$currentLicense = trim( get_site_option( 'wal_license_key' ) );
	}
	wp_localize_script('wallace-inline', 'wallaceInlineAdminInit', array(
		'currentLicense' => $currentLicense,
		'licenseActive' => wal_check_license() === 'valid',
		'connectionInfo' => array(
			'walRestURL' => esc_url_raw( rest_url('wallaceinline/v2/') ),
			'nonce' => wp_create_nonce('wp_rest'),
			'networkAdmin' => is_network_admin() || (is_multisite() && (home_url() === network_home_url())),
			'siteURL' => home_url(),
			'itemName' => WAL_ITEM_NAME,
		)
	));
	if(!is_network_admin()){
		echo '<h1>Wallace Inline License Management</h1><div id="wal-license-app"><p>Loading...</p></div>';

	}
	else{
		echo '<h1>Wallace Inline License Multisite Management</h1><p><em>Applies to all sites on the network.</em></p><div id="wal-license-app"><p>Loading...</p></div>';

	}
}




function wal_register_option() {
	// creates our settings in the options table
	register_setting('wal_license', 'wal_license_key', 'wal_sanitize_license' );
}
add_action('admin_init', 'wal_register_option');

function wal_sanitize_license( $new ) {
	$old = get_option( 'wal_license_key' );
	if( $old && $old != $new ) {
		delete_option( 'wal_license_status' ); // new license has been entered, so must reactivate
	}
	return $new;
}



/************************************
* this illustrates how to activate
* a license key
*************************************/

function edd_activate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['edd_license_activate'] ) ) {

		// run a quick security check
	 	if( ! check_admin_referer( 'wal_nonce', 'wal_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( get_option( 'wal_license_key' ) );


		// data to send in our API request
		$api_params = array(
			'edd_action' => 'activate_license',
			'license'    => $license,
			'item_name'  => urlencode( WAL_ITEM_NAME ), // the name of our product in EDD
			'url'        => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( WAL_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.' );
			}

		} else {

			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			if ( false === $license_data->success ) {

				switch( $license_data->error ) {

					case 'expired' :

						$message = sprintf(
							__( 'Your license key expired on %s.', 'wal-inline'),
							date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
						);
						break;

					case 'revoked' :

						$message = __( 'Your license key has been disabled.', 'wal-inline' );
						break;

					case 'missing' :

						$message = __( 'Invalid license.', 'wal-inline');
						break;

					case 'invalid' :
					case 'site_inactive' :

						$message = __( 'Your license is not active for this URL.', 'wal-inline');
						break;

					case 'item_name_mismatch' :

						$message = sprintf( __( 'This appears to be an invalid license key for %s.', 'wal-inline' ), WAL_ITEM_NAME );
						break;

					case 'no_activations_left':

						$message = __( 'Your license key has reached its activation limit.', 'wal-inline' );
						break;

					default :

						$message = __( 'An error occurred, please try again.', 'wal-inline' );
						break;
				}

			}

		}

		// Check if anything passed on a message constituting a failure
		if ( ! empty( $message ) ) {
			$base_url = admin_url( 'options-general.php?page=' . WAL_LICENSE_PAGE );
			$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );

			wp_redirect( $redirect );
			exit();
		}

		// $license_data->license will be either "valid" or "invalid"

		update_option( 'wal_license_status', $license_data->license );
		wp_redirect( admin_url( 'options-general.php?page=' . WAL_LICENSE_PAGE ) );
		exit();
	}
}
// add_action('admin_init', 'wal_activate_license');


/***********************************************
* Illustrates how to deactivate a license key.
* This will decrease the site count
***********************************************/

function edd_deactivate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['edd_license_deactivate'] ) ) {

		// run a quick security check
	 	if( ! check_admin_referer( 'wal_nonce', 'wal_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( get_option( 'wal_license_key' ) );


		// data to send in our API request
		$api_params = array(
			'edd_action' => 'deactivate_license',
			'license'    => $license,
			'item_name'  => urlencode( WAL_ITEM_NAME ), // the name of our product in EDD
			'url'        => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( WAL_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.' );
			}

			$base_url = admin_url( 'options-general.php?page=' . WAL_LICENSE_PAGE );
			$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );

			wp_redirect( $redirect );
			exit();
		}

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// $license_data->license will be either "deactivated" or "failed"
		if( $license_data->license == 'deactivated' ) {
			delete_option( 'wal_license_status' );
		}

		wp_redirect( admin_url( 'options-general.php?page=' . WAL_LICENSE_PAGE ) );
		exit();

	}
}
// add_action('admin_init', 'wal_deactivate_license');


/************************************
* this illustrates how to check if
* a license key is still valid
* the updater does this for you,
* so this is only needed if you
* want to do something custom
*************************************/

function wal_check_license() {

	global $wp_version;

	$license = trim( get_option( 'wal_license_key' ) );
	if(get_site_option('wal_license_key') !== false){
		$license = trim( get_site_option( 'wal_license_key' ) );
	}
	$networkLicense = get_site_option('wal_license_key') !== false;

	$api_params = array(
		'edd_action' => 'check_license',
		'license' => $license,
		'item_name' => urlencode( WAL_ITEM_NAME ),
		'url'       => $networkLicense ? network_home_url() : home_url()
	);

	// Call the custom API.
	$response = wp_remote_post( WAL_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	if ( is_wp_error( $response ) )
		return false;

	$license_data = json_decode( wp_remote_retrieve_body( $response ) );

	if( $license_data->license == 'valid' ) {
		return 'valid'; 
		// this license is still valid
	} else {
		return 'invalid'; 
		// this license is no longer valid
	}
}

function wal_update_message( $plugin_data, $response ) {
		// echo '<script>console.log(' . json_encode($response) . ');</script>';

	if ( 'invalid' == wal_check_license() ) {

		if(!is_multisite()){
			$message  = ' <a href="'. admin_url( 'options-general.php?page=' . WAL_LICENSE_PAGE ) .' "><b> '. esc_html__('Activate your license to receive updates', 'wal-inline') . '</a></b> ';
		}
		else{
			$message  = ' <a href="'. network_admin_url( 'settings.php?page=' . 'wallaceinline-network-license' ) .' "><b> '. esc_html__('Activate your license to receive updates', 'wal-inline') . '</a></b> ';

		}

		echo $message;
	}
}
add_action( 'in_plugin_update_message-wallace-inline/wallace-inline.php', 'wal_update_message', 1, 2 );

/**
 * This is a means of catching errors from the activation method above and displaying it to the customer
 */
function wal_admin_notices() {
	if ( isset( $_GET['sl_activation'] ) && ! empty( $_GET['message'] ) ) {

		switch( $_GET['sl_activation'] ) {

			case 'false':
				$message = urldecode( $_GET['message'] );
				?>
				<div class="error">
					<p><?php echo $message; ?></p>
				</div>
				<?php
				break;

			case 'true':
			default:
				// Developers can put a custom success message here for when activation is successful if they way.
				break;

		}
	}
}
add_action( 'admin_notices', 'wal_admin_notices' );
?>