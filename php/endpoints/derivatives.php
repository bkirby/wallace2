<?php
namespace WallaceInline;

use Rx\Observable;
use Rx\DisposableInterface;
use Rx\ObservableInterface;
use Rx\ObserverInterface;

function derivativesFromBeaverField($field){
    return Observable::create(function (ObserverInterface $observer) use ($field){
        $observer->onNext($field);

        if($field['field']['dataSource']['source'] === 'elementor'){
            if($field['field']['uiType'] === 'bgMedia'){
                $photoId = $field['data'];
                $field['field']['fieldUri'] = 'background_image url';
                $url = wp_get_attachment_image_src($photoId, 'large')[0];
                if(isset($url) && $url!==null){
                    $field['data'] = $url;
                }
                //default image size is large, otherwise get image_size attrib
                $observer->onNext($field);
            }
            $observer->onCompleted();
            return;
        }

        if($field['field']['dataSource']['source'] !== 'bb_field'){
            $observer->onCompleted();
            return;
        }

        


        switch($field['field']['moduleSlug']){
            
            case 'team':
            case 'image-icon':
            case 'uabb-photo':
            case 'pp-image':
            case 'photo':
            case 'info-box':
                if($field['field']['uiType'] === 'media'){
                    $photoId = $field['data'];
                    $field['field']['fieldUri'] = 'photo_src';
                    $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                    $observer->onNext($field);
                }
                break;
                    

            case 'pp-infobox':
                if($field['field']['uiType'] === 'media'){
                    $photoId = $field['data'];
                    $field['field']['fieldUri'] = 'image_select_src';
                    $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                    $observer->onNext($field);
                }
                break;

            case 'gsr-soulsections':
                if($field['field']['uiType'] === 'media'){
                    if($field['field']['fieldUri'] === 'singlesection_subsection_foreground_image'){
                        $photoId = $field['data'];
                        $field['field']['fieldUri'] = 'singlesection_subsection_foreground_image_src';
                        $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                        $observer->onNext($field);
                    }
                    else{

                    
                        $explode = explode(' ', $field['field']['fieldUri']);
                        array_pop($explode);

                        $photoId = $field['data'];
                        $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                        $field['field']['fieldUri'] = implode(' ', $explode) . ' multisection_subsection_foreground_image_src';
                        $observer->onNext($field);
                    }
                }
                break;

            case 'pp-team':
                if($field['field']['uiType'] === 'media'){
                    $photoId = $field['data'];
                    $field['field']['fieldUri'] = 'member_image_src';
                    $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                    $observer->onNext($field);
                }
                break;

            case 'row':
                $photoId = $field['data'];
                $field['field']['fieldUri'] = 'bg_image_src';
                $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                $observer->onNext($field);

                break;
            
            case 'pp-testimonials':
            case 'info-list':
                if($field['field']['uiType'] === 'media'){

                    $explode = explode(' ', $field['field']['fieldUri']);
                    array_pop($explode);

                    $photoId = $field['data'];
                    $field['field']['fieldUri'] = implode(' ', $explode) . ' photo_src'; // == replace last item in uri
                    $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                    $observer->onNext($field);
                }
                break;
            case 'pp-team':
                if($field['field']['uiType'] === 'media'){

                    $explode = explode(' ', $field['field']['fieldUri']);
                    array_pop($explode);

                    $photoId = $field['data'];
                    $field['field']['fieldUri'] = implode(' ', $explode) . ' content_photo_src'; // == replace last item in uri
                    $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                    $observer->onNext($field);
                }
                break;

            case 'advanced-tabs':
                if($field['field']['uiType'] === 'media'){

                    $explode = explode(' ', $field['field']['fieldUri']);
                    array_pop($explode);

                    $photoId = $field['data'];
                    $field['field']['fieldUri'] = implode(' ', $explode) . ' ct_photo_src'; // == replace last item in uri
                    $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                    $observer->onNext($field);
                }
                break;

            case 'pp-logos-grid':
                if($field['field']['uiType'] === 'media'){

                    $explode = explode(' ', $field['field']['fieldUri']);
                    array_pop($explode);

                    $photoId = $field['data'];
                    $field['field']['fieldUri'] = implode(' ', $explode) . ' upload_logo_grid_src'; // == replace last item in uri
                    $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                    $observer->onNext($field);
                }
                break;

            case 'pp-restaurant-menu':
                if($field['field']['uiType'] === 'media'){
                    $explode = explode(' ', $field['field']['fieldUri']);
                    array_pop($explode);

                    $photoId = $field['data'];
                    $field['field']['fieldUri'] = implode(' ', $explode) . ' menu_item_images_src'; // == replace last item in uri
                    $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                    $observer->onNext($field);
                }
                break;

            case 'adv-testimonials':
                if($field['field']['uiType'] === 'media'){
                    if($field['field']['moduleSettings']['tetimonial_layout'] === 'box'){
                        $photoId = $field['data'];
                        $field['field']['fieldUri'] = 'photo_noslider_src';
                        $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                        $observer->onNext($field);
                    }
                    else{
                        $explode = explode(' ', $field['field']['fieldUri']);
                        array_pop($explode);

                        $photoId = $field['data'];
                        $field['field']['fieldUri'] = implode(' ', $explode) . ' photo_src'; // == replace last item in uri
                        $field['data'] = \FLBuilderPhoto::get_attachment_data( $photoId )->url;
                        $observer->onNext($field);
                    }
                }
                break;
           
            // case 'pricing-table':
            //     $pricingField = preg_match('/pricing_columns 0 price/', $field['field']['fieldUri']);
            //     if(preg_match('/pricing_columns [0-9]+ price/', $field['field']['fieldUri']) === 1){
            //         $matches = '';
            //         preg_match('/<span(.*)>(.*)<\/span>/', $field['data'], $matches);
            //         $field['data'] = $matches[2];
            //         $field['field']['fieldUri'] = str_replace('price', 'duration', $field['field']['fieldUri']);
            //         $observer->onNext($field);
            //     }
            //     //if feature, regex match every <li> elem and create a field derivative for each
            //     if(preg_match('/pricing_columns ([0-9]+) features/', $field['field']['fieldUri'], $column) === 1){
            //         $matches = '';
            //         preg_match_all("|<[^>]+>(.*)</[^>]+>|U", $field['data'], $matches, PREG_SET_ORDER);
            //         foreach($matches as $key => $value){
            //             if($key === 0){
            //                 $field['data'] = 'wal_delete';
            //             }
            //             else{
            //                 $field['data'] = $value[1];
            //             }
            //             $field['field']['fieldUri'] = 'pricing_columns ' . $column[1] . ' features ' . $key;
            //             $observer->onNext($field);
            //         }
            //     }

           


                break;
        }
        $observer->onCompleted();
    });
    
}

?>