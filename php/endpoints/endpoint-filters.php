<?php

add_filter('wal_save_field', function($fieldData){

//if field is pricing table, strip out inner span elem from content to be saved
    if(in_array($fieldData['field']['moduleSlug'], array('pricing-table', 'pp-pricing-table', 'pricing-box')) && strpos($fieldData['field']['fieldUri'], 'price') !== 0){
        $data = $fieldData['data'];
        $stripInnerSpan = preg_replace('/<span(.*)<\/span>/', '', $data);
        $stripOuterP = preg_replace(array('/<p>/', '/<\/p>/'), '', $stripInnerSpan);
        $fieldData['data'] = $stripOuterP;

        return $fieldData;
    }

    if(in_array($fieldData['field']['moduleSlug'], array('price-table')) && $fieldData['field']['fieldUri'] === 'price'){
        $data = $fieldData['data'];
        
        
        return $fieldData;
        
    
    }


    // if(strpos($fieldData['field']['fieldUri'], 'features') !== 0){
        
    //     return null;
    // }

    return $fieldData;

    //if feature return null
}, 10, 1);

?>