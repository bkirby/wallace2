<?php
namespace WallaceInline;

require_once('derivatives.php');
require_once('endpoint-filters.php');

use Rx\Observable;
use React\EventLoop\Factory as EventLoopFactory;
use Rx\Scheduler;

function publish($request){


	

	$loop = EventLoopFactory::create();

	Scheduler::setDefaultFactory(function() use($loop){
		return new Scheduler\EventLoopScheduler($loop);
	});


	$fields = Observable::fromArray(json_decode($request->get_body(), true))
	->concatMap(function($field){
		return deriveFields($field);
	})
	->map(function($field){
		return apply_filters('wal_save_field', $field);
	})
	->filter(function($field){
		return !is_null($field);
	});

	//determine  then filter user_has_cap to give edit_others_posts
	
	publishElementorFields($fields);
	publishBeaverFields($fields);
	publishPodsFields($fields);
	publishAcfFields($fields);
	publishWpFields($fields);
	$loop->run();




}
add_action('rest_api_init', function() {

	

	register_rest_route('wallaceinline/v2', 'publish', array(
		'methods' => 'POST',
		'callback' => 'WallaceInline\publish',
		'permission_callback' => function() {
			return true;
		}
	));
});

function deriveFields($field){
	return derivativesFromBeaverField($field);
}



function publishElementorFields($fields){
	// $elementor->ajax_save();

	$fields->filter(function($field){
		if($field['field']['dataSource']['source'] === 'elementor'){
			return true;
		}
		return false;
	})
	->groupBy(function($field){
		return $field['field']['moduleId'];
	})
	->map(function($module){

		$price = false;
		$moduleId = $module->getKey();

		return $module->map(function($field) use (&$price, $moduleId){
			
			if($field['field']['moduleSlug'] === 'price-table' ){
				//get the existing price data
				
				if($field['field']['altUri'] === 'integer'){
					$elements = getRawElementorData($field['field']['dataSource']['postId']);
					$element = elementorModuleFromId($moduleId, $elements);
					if($price === false){
						$price = $element['settings']['price'];
					}

					$price = preg_replace('/[^.]*/', $field['data'] . '.', $price, 1);
					$field['data'] = $price;//just the integer part changed from price
				}
				
				if($field['field']['altUri'] === 'fractional'){
					$elements = getRawElementorData($field['field']['dataSource']['postId']);
					$element = elementorModuleFromId($moduleId, $elements);
					if($price === false){
						$price = $element['settings']['price'];
					}

					$price = preg_replace('/\.(.*)/', '.' . $field['data'], $price, 1);
					$field['data'] = $price; //just the fractional part changed from price
				}

			}

			
			// if($fieldData['field']['moduleSlug'] === 'pricing table'){ //todo: change this to be right
			// $integerPrice = 0;
			// $fractionalPrice = 0;

			// forEach($moduleFields as $field){
			// 	//set respective prices

			// }

			// //update the field with the price uri
		// }
			return $field;
		});
		// ->subscribe(function($result){
		// 	return $result;
		// });
		
		
		// return $moduleFields;
	})
	
	
	
	->mergeAll()
	->groupBy(function($field){
		return $field['field']['dataSource']['postId'];
	})
	->subscribe(function($postFields){
		$postId = $postFields->getKey();
		$elements = getRawElementorData($postId);
		// $settings = getRawElementorSettingsData($postId);
		//group by module, handle price transformation

		$postFields->toArray()
		->subscribe(function($fields) use ($elements, $postId, $settings){
			$modifiedElements = array_reduce($fields, function($acc, $field){
				return array_map($recursive = function($item) use (&$recursive, $field){
					//just check for id? get only modified elems, merge with base elems
					// if(isset($item['id']) && (count($item['elements']) < 1 || $item['elType']==='section')){
					if(isset($item['id'])){
						if($field['field']['moduleId'] === $item['id']){
							$topLevelSettingName = explode(' ', $field['field']['fieldUri'])[0];

							$item['settings'][$topLevelSettingName] = setTopLevelSettingObject(
							$item['settings'], $field['field']['fieldUri'], $field['data'])[$topLevelSettingName];
						}
					}
					// }
					if(is_array($item)){
						return array_map($recursive, $item);
					}
					return $item;
				}, $acc);
			}, $elements);


			if(userHasPublishPermission($postId)){
					//not right post id why?
					

				add_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10, 3);

				$elementor = \Elementor\Plugin::$instance->documents;
				$settings = $elementor->get($postId)->get_settings();
				
				$elementor->ajax_save(array(
					'status' => 'publish',
					'elements' => $modifiedElements,
					'settings' => $settings,
					'editor_post_id' => $postId
				));

				remove_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10);
			}
			
		});
	});
}
	



function publishWpFields($fields){

	$fields->filter(function($field){
		if($field['field']['dataSource']['source'] === 'title' || $field['field']['dataSource']['source'] === 'content'){
			return true;
		}
		return false;
	})
	->subscribe(function($field){
		$postId = $field['field']['dataSource']['postId'];

		if(userHasPublishPermission($postId)){

			add_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10, 3);

			switch($field['field']['dataSource']['source']){

				case 'title':
					$post = array(
						'ID' => $field['field']['dataSource']['postId'],
						'post_title' => $field['data']
					);
					wp_update_post($post);
		
				break;
				
				case 'content':

					$post = array(
						'ID' => $field['field']['dataSource']['postId'],
						'post_content' => $field['data']
					);

					wp_update_post($post);

				break;
			}

			remove_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10);
		}

	});
}

function publishAcfFields(Observable $fields){
	$fields->filter(function($field){
		if(in_array($field['field']['dataSource']['source'], array('acf', 'acf_photo'))){
			return true;
		}
		return false;
	})
	->subscribe(function($field){
		$connectionSettings = (array)$field['field']['dataSource']['connection']['settings'];
		$acfFieldName = $connectionSettings['name'];
		$data = $field['data'];
		$postId = $field['field']['dataSource']['postId'];
		if(userHasPublishPermission($postId)){

			add_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10, 3);

			switch($field['field']['dataSource']['source']){
				case('acf'):
					update_field($acfFieldName, $data, $postId);
					break;
				case('acf_photo'):
					update_field($acfFieldName, $data, $postId);

					break;
			}
			remove_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10);
		}
		//check for image field type, update accordingly
	});
}


function publishPodsFields(Observable $fields){
	$fields->filter(function($field){
		if(in_array($field['field']['dataSource']['source'], array('pods_display', 'pods_photo'))){
			return true;
		}
		return false;
	})
	->subscribe(function($field){
		$podName = $field['field']['dataSource']['pod'];
		$pod = \PodsAPI::init($podName);
		$podFieldName = $field['field']['dataSource']['connection']['settings']['field'];
		$data = $field['data'];
		$postId = $field['field']['dataSource']['postId'];
		if(userHasPublishPermission($postId)){

			add_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10, 3);
			
			switch($field['field']['dataSource']['source']){
				case('pods_display'):
					$pod->save_pod_item(array('pod' => $podName, 'id' => $postId, 'data' => array($podFieldName => $data)));
					break;
				case('pods_photo'):
					$pod->save_pod_item(array('pod' => $podName, 'id' => $postId, 'data' => array($podFieldName => $data)));
					break;
			}
			remove_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10);
		}

	});
}


function publishBeaverFields(Observable $fields){
	//filter to include only beaver data sources
	//group by module id
	//subscribe to each group of module ids
	//publish

	

	$fields->filter(function($field){
		if($field['field']['dataSource']['source'] === 'bb_field'){
			return true;
		}
		return false;
	})
	->groupBy(function($field){
		return $field['field']['dataSource']['postId'];
	})
	//take module grouping, find the template post id it belongs to and pass it along
	->subscribe(function($postFields){
		
		$postId = $postFields->getKey();
		$layoutData = \FlBuilderModel::get_layout_data( 'published', $postId );
		$groups = $postFields->reduce(function($acc, $field){
			$topLevelSettingName = explode(' ', $field['field']['fieldUri'])[0];
			$moduleId = $field['field']['moduleId'];

			$acc[$moduleId]->settings->$topLevelSettingName = setTopLevelSettingObject(
				$acc[$moduleId]->settings, $field['field']['fieldUri'], $field['data'])[$topLevelSettingName];

			return $acc;
		}, $layoutData);

		$groups->subscribe(function($result) use ($postId){
			$test = 1;
			if(userHasPublishPermission($postId)){

				add_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10, 3);

				
				

				\FlBuilderModel::set_post_id($postId);

				// Save the template data.
				\FlBuilderModel::update_layout_data( $result, 'published', $postId );
				\FlBuilderModel::update_layout_data( $result, 'draft', $postId );

				// Delete the template asset cache.
				\FlBuilderModel::delete_all_asset_cache( $postId );
				\FlBuilderModel::delete_node_template_asset_cache( $postId );
				remove_filter('user_has_cap', 'WallaceInline\givePublishPermission', 10);
			}

		});
		
	});
}
function hasPageOverride($postId) {
	$user = wp_get_current_user()->data;
	if(isset($postId) && isset($user->ID)){
		$permissionKey = 'user_ids';
		$uac = json_decode(get_option('wal-user-access'));
		$uacPost = isset($uac->$postId) ? $uac->$postId : null;
		$usersWithOverride = $uacPost !== null ? $uacPost->$permissionKey : null;
		// $usersWithOverride = isset((json_decode(get_option('wal-user-access'))->$postId)[$permissionKey]) ? json_decode(get_option('wal-user-access')->$postId->$permissionKey) : null;
		
		if($usersWithOverride !== null){
			forEach($usersWithOverride as $id){
				$id = (string) $id;
				if($id === $user->ID){
					return true;
				}
			}
		}
		return false;
	}
	else{
		return false;
	}
}
function userHasPublishPermission($postId){
	
	if(current_user_can('wal_limited_access') || current_user_can('wal_full_access') || hasPageOverride($postId)){
		return true;
	}
	return false;
}

function givePublishPermission($allcaps, $caps, $args){
	$allcaps['edit_others_posts'] = true;
	$allcaps['edit_published_posts'] = true;
	$allcaps['edit_posts'] = true;
	$allcaps['edit_pages'] = true;
	$allcaps['edit_others_pages'] = true;
	$allcaps['edit_published_pages'] = true;



	return $allcaps;
}
	
		

function setTopLevelSettingObject($settings, $uri, $valueToSet){

	// array_pop($uri);
	$uriArray = explode(' ', $uri);

	if(sizeof($uriArray)===1){
		return array($uri => $valueToSet);
	}

	$slices = array_reduce($uriArray, function($acc, $fieldName) use($valueToSet){ //returns array of sliced arrays to be nested
		if(is_object($acc[0])){
			$acc[0] = (array)$acc[0];
		}
		$layer = $acc[0][$fieldName];
		if(is_object($layer)){
			$layer = (array)$layer;
		}
		if(!is_array($layer)){
			$acc[0][$fieldName] = $valueToSet;
		}
		else{
			array_unshift($acc, $layer);
		}
		return $acc;
	}, array($settings));

	$reconstructed = array_reduce($slices, function($acc, $slice){
		if($acc[1] === 'init'){
			array_shift($acc[0]);
			$acc[1] = $slice;
			return $acc;
		}
		$slice[$acc[0][0]]  = $acc[1];
		$acc[1] = $slice;
		array_shift($acc[0]);
		return $acc;
	}, array(array_reverse($uriArray), 'init'));

	return $reconstructed[1];
}

function wi_update_user_settings($request){
	$request_data = $request->get_json_params();
	$post_id = $request->get_header('X-Post-Id');
	$access_data = (array) json_decode(get_option('wal-user-access'), true);
	if(!isset($access_data)){
	  $access_data = array();
	}
	if(!isset($access_data[$post_id])){
	  $access_data[$post_id] = array();
	}
	// if(!isset($access_data[$post_id])){
	//   $access_data['full'] = array();
	// }
	//  if(!isset($access_data[$post_id])){
	//   $access_data['limited'] = array();
	// }
  
	if(isset($request_data['limited_role_names'])){
  
	  $limited_role_names = $request_data['limited_role_names'];
	  $full_role_names = $request_data['full_role_names'];
	  // return $limited_role_names;

	  if ( ! function_exists( 'get_editable_roles' ) ) {
		require_once ABSPATH . 'wp-admin/includes/user.php';
	}
  
	  foreach(get_editable_roles() as $registeredRoleName => $registeredRole){
		$registeredRole = get_role($registeredRoleName);
		$roleMatched = false;
		foreach($limited_role_names as $limited_role_name){
		  if($limited_role_name === $registeredRoleName){
			$registeredRole->add_cap('wal_limited_access');
			$registeredRole->remove_cap('wal_full_access');
			$roleMatched = true;
		  }
		  
		}
		foreach($full_role_names as $full_role_name){
		  if($full_role_name === $registeredRoleName){
			$registeredRole->add_cap('wal_full_access');
			$registeredRole->remove_cap('wal_limited_access');
			$roleMatched = true;
		  }
		  
		}
		
		if(!$roleMatched){
			$registeredRole->remove_cap('wal_limited_access');
			$registeredRole->remove_cap('wal_full_access');
  
		}
		if(get_option( 'wal-user-access', false) === false){
		  update_option('wal-user-access', true);
		}
		
	  }
	}
	if(isset($request_data['user_ids'])){
  
	$access_data[$post_id]['user_ids'] = $request_data['user_ids'];
	update_option( 'wal-user-access', json_encode($access_data));
	// return json_decode(get_option('wal-user-access'), true);
	}
  
  
  }
  add_action( 'rest_api_init', function () {
	register_rest_route( 'wallaceinline/v2', 'access', array(
	  'methods'  => 'POST',
	  'callback' => 'WallaceInline\wi_update_user_settings',
	  'permission_callback' => function () {
		return current_user_can('wal_full_access');
	  }
	) );
  } );
  
  
  
  
  function wi_lock_field($request){
	$request_data = $request->get_json_params();
	$lock_data = (array) json_decode(get_option('wal-locked'), true);
	if(!$lock_data){
	  $lock_data = array();
	}
	if(!isset($lock_data[$request_data['module_id']])){
	  $lock_data[$request_data['module_id']] = array();
	}
	$lock_data[$request_data['module_id']][$request_data['uri']] = $request_data['locked'];
  
  
	update_option( 'wal-locked', json_encode($lock_data));
	return json_decode(get_option('wal-locked'), true);
  
	
  }
  
  function wi_save_settings( $node_id = null, $settings = null ) {
	  // return FlBuilderModel::get_node_status();
	  // return FlBuilderModel::get_layout_data( 'published' );
  
	  $node = FlBuilderModel::get_node( $node_id );
	  
	  if($node === null){
		exit;
	  }
	  $new_settings   = (object) array_merge( (array) $node->settings, (array) $settings );
	  $template_post_id   = FlBuilderModel::is_node_global( $node );
  
	  // Process the settings.
	  $new_settings = FlBuilderModel::process_node_settings( $node, $new_settings );
  
	  // Save the settings to the node.
	  $data = FlBuilderModel::get_layout_data();
	  $data[ $node_id ]->settings = $new_settings;
  
	  // Update the layout data.
	  FlBuilderModel::update_layout_data( $data );
  
	  // Save settings for a global node template?
	  if ( $template_post_id && ! FlBuilderModel::is_post_node_template() ) {
  
		// Get the template data.
		$template_data = FlBuilderModel::get_layout_data( 'published', $template_post_id );
  
		// Update the template node settings.
		$template_data[ $node->template_node_id ]->settings = $new_settings;
  
		// Save the template data.
		FlBuilderModel::update_layout_data( $template_data, 'published', $template_post_id );
		FlBuilderModel::update_layout_data( $template_data, 'draft', $template_post_id );
  
		// Delete the template asset cache.
		FlBuilderModel::delete_all_asset_cache( $template_post_id );
		FlBuilderModel::delete_node_template_asset_cache( $template_post_id );
	  }
  
	  // Return the processed settings and new layout.
	  return array(
		'node_id'  => $node->node,
		'settings' => $new_settings,
		// 'layout'   => FLBuilderAJAXLayout::render(),
	  );
	}
  
  
  add_action( 'rest_api_init', function () {
	register_rest_route( 'wallaceinline/v2', 'lock', array(
	  'methods'  => 'POST',
	  'callback' => 'WallaceInline\wi_lock_field',
	  'permission_callback' => function () {
		return current_user_can('wal_full_access');
	  }
	) );
	} );
	

function wi_license_activate($request){
	//$license, $(de)activate? , $itemName, $multisite? , 
	//post to license server
	//save in option and return result if successful
	//return error reason if not
	$request_data = $request->get_json_params();

	$payload = array(
		'edd_action' => $request_data['edd_action'],
		'item_name' => $request_data['item_name'],
		// 'item_name' => 'Wallace Inline',
		'license' => $request_data['edd_action'] === 'deactivate_license' ? (get_site_option('wal_license_key') !== false ? get_site_option('wal_license_key') : get_option('wal_license_key')) : $request_data['license'],
		'url' => $request_data['url']
	);

	$licenseResponse = wp_remote_post( 'https://wallaceinline.com/edd', array( 'timeout' => 15, 'body' => $payload) );
	$responseBody = json_decode(wp_remote_retrieve_body($licenseResponse), true); 
	
	if($request_data['edd_action'] === 'deactivate_license'){
		if($responseBody['success']){
			$responseBody['message'] = __( 'License deactivated', 'wal-inline' );
				delete_site_option('wal_license_key');
				delete_option('wal_license_key');
			return $responseBody;
		}
		$responseBody['message'] = __( 'An error occurred, please try again.', 'wal-inline' );
		return $responseBody;
	}

	if($responseBody['success']){
		if($request_data['networkAdmin'] && in_array($responseBody['price_id'], ['5', '6'])){
			$responseBody['message'] = __("Your license isn't valid for multisite activation. Visit your account page at https://wallaceinline.com/my-account to upgrade your license.", 'wal-inline');
			$deactivate = array(
				'edd_action' => 'deactivate_license',
				'item_name' => $request_data['item_name'],
				'license' => $request_data['license'],
				'url' => $request_data['url']
			);
			wp_remote_post( 'https://wallaceinline.com/edd', array( 'timeout' => 15, 'body' => $deactivate) );
			return $responseBody;
		}
		//save license in options
		if($request_data['networkAdmin']){
			update_site_option('wal_license_key', $payload['license']);
		}
		else{
			update_option('wal_license_key', $payload['license']);
		}
		$responseBody['message'] = "Success! Thanks for being a Wallace Inline customer!";
	  	return $responseBody;
	}
	switch( $responseBody['error'] ) {
		case 'expired' :
			$responseBody['message'] = sprintf(
				__( 'Your license key expired on %s.', 'wal-inline'),
				date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
			);
			break;
		case 'revoked' :
			$responseBody['message'] = __( 'Your license key has been disabled.', 'wal-inline' );
			break;
		case 'missing' :
			$responseBody['message'] = __( 'Invalid license.', 'wal-inline');
			break;
		case 'invalid' :
		case 'site_inactive' :
			$responseBody['message'] = __( 'Your license is not active for this URL.', 'wal-inline');
			break;
		case 'item_name_mismatch' :
			$responseBody['message'] = sprintf( __( 'This appears to be an invalid license key for %s.', 'wal-inline' ), WAL_ITEM_NAME );
			break;
		case 'no_activations_left':
			$responseBody['message'] = __( 'Your license key has reached its activation limit.', 'wal-inline' );
			break;
		default :
			$responseBody['message'] = __( 'An error occurred, please try again.', 'wal-inline' );
			break;
	}
	return $responseBody;
	



	return $licenseResponse;
}

	add_action('rest_api_init', function(){
		register_rest_route('wallaceinline/v2', 'license', array(
			'methods' => 'POST',
			'callback' => 'WallaceInline\wi_license_activate',
			'permission_callback' => function (){
				return current_user_can('manage_options');
			}
		));
	});

?>