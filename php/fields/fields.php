<?php
namespace WallaceInline;

require_once('default-filters.php');
require_once('fromBeaverBuilder.php');
require_once('fromElementor.php');

use Rx\Observable;
use Rx\ObserverInterface;
use Rx\SchedulerInterface; 
use Rx\Operator\OperatorInterface;
use Rx\Operator\toWallaceFieldStream;
use WP_REST_Request;

final class FieldFactory{
	private static $hasFields = false;
	private static $beaverFields;
	private static $elementorFields;
	private static $beaverRows = [];

	private function __construct(){
		$this->setup();
	}

	public static function getInstance(){
		static $inst = null;
        if ($inst === null) {
            $inst = new FieldFactory();
        }
        return $inst;
	}

	public function currentPageHasEditableFields(){
		return self::$hasFields;
	}

	public function getFields(){
		
		return array_merge(self::$beaverFields, self::$beaverRows, self::$elementorFields);
	}

	public function getElementorFields(){
		return self::$elementorFields;
	}

	private function setup(){

		$beaverBuilderSource = Observable::create(function (ObserverInterface $observer){
			add_action( 'fl_builder_after_render_module', function($module) use ($observer){
				// print '<script>console.log(' . json_encode($module) . ');</script>';

				$observer->onNext($module);
			}, 10, 1);

			add_action( 'fl_builder_after_render_row_bg', function($row) use ($observer){
				// print '<script>console.log(' . json_encode($row) . ');</script>';

				$observer->onNext($row);
			}, 10, 1);
			
			add_action('wp_footer', function() use($observer){
				// print '<script>console.log(' . json_encode('done') . ');</script>';
				$observer->onCompleted();
			}, 8, 0);
		});

		$elementorSource = Observable::create(function (ObserverInterface $observer){
			//hook into elementor when it renders an element, add some markup to id it 
			add_action('elementor/frontend/widget/before_render', function ( \Elementor\Element_Base $module ) use ($observer){
				print '<script>console.log(' . json_encode($module->get_data()) . ');</script>';
				$observer->onNext($module);
				
			});

			// add_action('elementor/frontend/section/before_render', function ( \Elementor\Element_Base $module ) use ($observer){
			// 	// print '<script>console.log(' . json_encode($module->get_data()) . ');</script>';
			// 	$observer->onNext($module);
			// });

			add_action('wp_footer', function() use($observer){
				$observer->onCompleted();
			}, 8, 0);

		});

		// $rowStream = Observable::create(function (ObserverInterface $observer){
		
		// 	add_action( 'fl_builder_after_render_row_bg', function($row) use ($observer){
		// 		// print '<script>console.log(' . json_encode($row) . ');</script>';

		// 		$observer->onNext($row);
		// 	}, 10, 1);
			
		// 	add_action('wp_footer', function() use($observer){
		// 		// print '<script>console.log(' . json_encode('done') . ');</script>';
		// 		$observer->onCompleted();
		// 	}, 8, 0);
		// });


		// $beaverBuilderStream->subscribe(function($item){
		// 	print '<script>console.log(' . json_encode($item) . ');</script>';

		// });
		

		// $beaverBuilderStream
		// 	->take(1)	
		// 	->subscribe(function($item){ //subscribe to only first item then shutdown and give hasFields = true
		// 		self::$hasFields = true;
		// 		// print '<script>console.log(' . json_encode(self::$hasFields) . ');</script>';
		// 	});
		
		$elementorFieldStream = fromElementor($elementorSource);
		$elementorFieldStream
			->toArray()
			->subscribe(function($fields){
				print '<script>console.log(' . json_encode($fields) . ');</script>';

				self::$elementorFields = $fields;
				if(sizeof(self::$elementorFields) > 0){
					self::$hasFields = true;
				}
			});

		// $rowStream
		// 	->toArray()
		// 	->subscribe(function($fields){
		// 		self::$beaverRows = $fields;
		// 		// print '<script>console.log(' . json_encode(self::$rows) . ');</script>';


		// 	});

		$beaverBuilderStream = fromBeaverBuilder($beaverBuilderSource)
			->toArray()
			->subscribe(function($fields){

				// $fieldsToPublish = array_map(function($field){
				// 	if($field['uiType'] === 'media'){
				// 		return array('field' => $field, 'data' => '40'); //40 38
				// 	}
				// 	if($field['uiType'] === 'icon'){
				// 		return array('field' => $field, 'data' => 'fas fa-arrow-circle-up'); // fas fa-arrow-circle-up
				// 	}
				// 	if($field['uiType'] === 'link'){
				// 		return array('field' => $field, 'data' => 'https://twitter.com'); // https://google.com
				// 	}

				// 	return array('field' => $field, 'data' => (string)random_int(1, 10000));
				// }, $fields);

				// $request = new WP_REST_Request('POST', '/wallaceinline/v2/publish');
				// $request->set_body(json_encode($fieldsToPublish));
				// $request->set_header('X-Post-Id', '2');
				// $response = rest_do_request($request);

				self::$beaverFields = $fields;
				if(sizeof(self::$beaverFields) > 0){
					self::$hasFields = true;
				}

				// print '<script>var wallaceInline = '. json_encode($fields) .'</script>';
				// print '<script>console.log(' . json_encode($fields) . ');</script>';
				// print '<script>console.log(' . json_encode($response) . ');</script>';

			});
	}




}









?>