<?php
namespace WallaceInline;

require_once('declared-modules.php');

use Rx\Observable;
use Rx\DisposableInterface;
use Rx\ObservableInterface;
use Rx\ObserverInterface;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use WallaceInline\ModuleDefinitions;


function fromElementor($observable){
    if(!class_exists("Elementor\Plugin") || is_admin() || sizeof(get_post_meta( get_the_ID(), '_elementor_data', false)) < 1){
		return $observable;
	}
    return $observable->concatMap(function($module){


        $moduleData = $module->get_data();

        $moduleDefs = ModuleDefinitions::getElementorModules();
        $matchedModuleDef = isset($moduleDefs[$moduleData['widgetType']]) ? $moduleDefs[$moduleData['widgetType']] : null;

        if($matchedModuleDef === null){
            return Observable::empty();
        }

        $newField = function($fieldDef) use ($moduleData, $module){
            return array(
                'moduleRef' => $module,
                'fieldDef' => $fieldDef,
                'moduleId' => $moduleData['id'],
                'moduleSlug' => $moduleData['widgetType'],
                // 'moduleSettings' => $moduleData['settings'],
                'fieldUri' => $fieldDef['uri'],
                'altUri' => isset($fieldDef['alturi']) ? $fieldDef['alturi'] : false,
                'fieldSelector' => "[data-id='" . $moduleData['id'] . "'] " . $fieldDef['selector'],
                'uiType' => $fieldDef['uiType'],
                // 'dataSource' => array('source' => 'elementor', 'postId' => get_the_ID())
                'dataSource' => getDataSource($moduleData['id'], $moduleData)

                // 'locked' => self::isFieldLocked($moduleData['id'], $fieldDef['uri'])
            );
        };

        $fieldDefToFields = function($fieldDef, $uriSegments, $settings) use (&$fieldDefToFields, $newField){
            if(sizeof($uriSegments) === 0){
                return $newField($fieldDef);
            }
            $nextUriSegment = array_shift($uriSegments);

            if($nextUriSegment === '{order}'){
                $indices = array_map(function($index){return $index;}, array_keys($settings));
                return array_reduce($indices, function($acc, $index) use ($fieldDef, $uriSegments, $settings, $nextUriSegment, &$fieldDefToFields){
                    $fieldDef['uri'] = substr_replace($fieldDef['uri'], $index, strpos($fieldDef['uri'], '{order}'), strlen('{order}'));
                    $fieldDef['selector'] = substr_replace($fieldDef['selector'], '{'.$index.'}', strpos($fieldDef['selector'], '{order}'), strlen('{order}'));
                    
                    $next = $fieldDefToFields($fieldDef, $uriSegments, $settings[$index]);
                    if(isset($next['fieldUri'])){
                        $acc[] = $next; //single field is *pushed* to accumulator array
                        return $acc;
                    }
                    return array_merge($acc, $next); //array of fields are *merged* with accmulator array
                }, []);
            }
            //if image, set up initial value in settings
            if($nextUriSegment === 'image' && !isset($settings['image'])){
                $settings['image'] = 0;
            }
            return $fieldDefToFields($fieldDef, $uriSegments, $settings[$nextUriSegment]);
        };

        $matchedDefs = array_map(function ($selector, $matchedModuleDef){$matchedModuleDef['selector']=$selector; return $matchedModuleDef;}, array_keys($matchedModuleDef), $matchedModuleDef); //array_reduce can't access keys so move the selector key to the array body
        $fields = array_reduce($matchedDefs, function($acc, $def) use ($fieldDefToFields, $moduleData){
            $newFields = $fieldDefToFields($def, explode(' ', $def['uri']), $moduleData['settings']);
            if(isset($newFields['fieldUri'])){
                $acc[] = $newFields; //single field is *pushed* to accumulator array
                return $acc;
            }
            return array_merge($acc, $newFields);
        }, []);
        
        $fieldsWithLockData = array_map(function($field){
            $lockedOption = (array) json_decode(get_option('wal-locked'), true);;
            $moduleId = $field['moduleId'];
            $fieldUri = $field['fieldUri'];

            if(!isset($lockedOption)){
                $field['locked'] = false;
            }
            else if(!isset($lockedOption[$moduleId])){
                $field['locked'] = false;
            }
            else if(!isset($lockedOption[$moduleId][$fieldUri])){
                $field['locked'] = false;
            }

            else{
                $field['locked'] = $lockedOption[$moduleId][$fieldUri];
            }
            return $field;
        }, $fields);


        $fieldsWithParents = array_map(function($field){
            $parent = $field['moduleRef'];
            $elements = getRawElementorData($field['dataSource']['postId']);
            
            $elemsWithParents = getParentModules($elements);

            $parentId = elementorModuleFromId($field['moduleId'], $elemsWithParents)['parent'];
            $field['parent'] = $parentId;
            return $field;
            // return getElemRowId();
        }, $fieldsWithLockData);

        $fieldsWithRowId = array_map(function($field) use ($fieldsWithParents){
            $elements = getRawElementorData($field['dataSource']['postId']);
            
            $elemsWithParents = getParentModules($elements);

            //recursive function goes here, look at parents until you find row then return its id
            $recursive = function($elemId, $elems) use(&$recursive, $fieldsWithParents){
                $elem = elementorModuleFromId($elemId, $elems);

                if($elem['elType'] === 'section'){
                    return $elem['id'];
                }

                if(isset($elem['parent'])){
                    return $recursive($elem['parent'], $elems);
                }

                return 'none';
            };

            $field['rowId'] = $recursive($field['moduleId'], $elemsWithParents);

            return $field;
        }, $fieldsWithParents);

        $fieldsWithLinkData = array_map(function($field) use ($fieldsWithRowId){
            //if uitype is link, get link from uri and add to object
            if($field['uiType'] !== 'link'){
                return $field;
            }
            $data = $field['moduleRef']->get_data();

            $link = $data['settings']['link']['url'];
            $field['link'] = $link;
            $field['context'] = array_map(function($contextDef) use($field){
                return "[data-id='" . $field['moduleId'] . "'] " . $contextDef;
            }, $field['fieldDef']['context']);
            
            return $field;

            //add link and context to object
        }, $fieldsWithRowId);

        return Observable::fromArray($fieldsWithLinkData);
    })
    ->concat(Observable::fromArray(getSectionElems(get_the_ID()))
        ->map(function($sectionElem){
            return array(
                'moduleRef' => $sectionElem,
                'moduleId' => $sectionElem['id'],
                'moduleSlug' => 'section',
                'fieldUri' => 'background_image id',
                'fieldSelector' => "[data-id='" . $sectionElem['id'] . "'] ",
                'uiType' => 'bgMedia',
                'dataSource' => array('source' => 'elementor', 'postId' => get_the_ID()),
                // 'locked' => self::isFieldLocked($moduleData['id'], $fieldDef['uri'])
            );
        })
    );
    

}

function getDataSource($moduleId, $moduleData){
    //get raw data for current id, simple regex search to see if present, if it's not use the custom sql query to see which post id it belongs to, error on more than 1 result 
    $currentPostElements = json_encode(getRawElementorData(get_the_ID()), JSON_PARTIAL_OUTPUT_ON_ERROR);
    $moduleInCurrentPost = preg_match('/' . $moduleId . '/', $currentPostElements);

    if($moduleInCurrentPost){
        return array('source' => 'elementor', 'postId' => get_the_ID(), 'external' => false);
    }
    else{
        $ids = $GLOBALS['wpdb']->get_results("SELECT post_id, post_type
        FROM wp_13_postmeta
        INNER JOIN wp_13_posts ON wp_13_posts.ID = wp_13_postmeta.post_id 
        WHERE wp_13_postmeta.meta_key = '_elementor_data' AND wp_13_postmeta.meta_value REGEXP '7e6737b' AND wp_13_posts.post_type <> 'revision'");

        $postId = $ids[0]->post_id;
        $postType = $ids[0]->post_type;
        return array('source' => 'elementor', 'postId' => $postId, 'postType' => $postType, 'external' => true);
        //add context info here 

    }


}

function getSectionElems($postId){

    
    $elements = getRawElementorData($postId);
        $elementsArray = []; 
        if(!is_array($elements)){
            return [];
        }
        array_map($recursive = function($node) use (&$recursive, &$elementsArray){
            $elementsArray[] = $node;

            if(sizeof($node['elements']) < 1){
                return;
            }

            return array_map(function($child) use($recursive){
                $recursive($child);
            }, $node['elements']);
        }, $elements);

        $sections = array_filter($elementsArray, function($elem){
            return $elem['elType'] === 'section';
        });
        return $sections;
}

function getElemRowId($field, $elems){
    $elem = elementorModuleFromId($field['id'], $elems);
    $parentElem = elementorModuleFromId($field['parent'], $elems); //probably shouldn't assume parent is same post id of child
    return;
    //if parent is row, return its id
    //else getRowId of parent
}



function elementorModuleFromId($id, $elements){
    // $elements = json_decode(get_post_meta( $postId, '_elementor_data', true), true);
    $elementsArray = []; 
      array_map($recursive = function($node) use (&$recursive, &$elementsArray){
        $elementsArray[] = $node;

        if(sizeof($node['elements']) < 1){
            return;
        }

        return array_map(function($child) use($recursive){
            $recursive($child);
        }, $node['elements']);
    }, $elements);

    foreach($elementsArray as $elem){
        if($elem['id'] === $id){
            return $elem;
        }
    }

    return null;
}

function getParentModules($elements){
    
    return array_map($recursive = function($node) use(&$recursive){
        if(sizeof($node['elements']) < 1){
            return $node;
        }

        $parentId = $node['id'];

        $node['elements'] = array_map(function($child) use($parentId, $recursive){
            sizeof($child['elements']);
            $child['parent'] = $parentId;
            return $recursive($child);
        }, $node['elements']);

        return $node;
    }, $elements);
}

function getRawElementorData($postId){
   
    $postMeta = get_post_meta( $postId, '_elementor_data', false)[0];
    if(is_array($postMeta)){
        return $postMeta;
    }
    return json_decode($postMeta, true);
}

function getRawElementorSettingsData($postId){
   
    $postMeta = get_post_meta( $postId, '_elementor_page_settings', false)[0];
    if(is_array($postMeta)){
        return $postMeta;
    }
    return json_decode($postMeta, true);
}
?>