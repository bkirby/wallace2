<?php



add_filter('wal_ignore_shortcodes', function($default){
    return $default; //default is true, fields containing shortcodes are ignored by default, override this filter to change
}, -10, 1);




add_filter('wal_pre_filter', function($field){

    preg_match_all( '/' . get_shortcode_regex() . '/', wal_getFieldDataFromUri($field['fieldUri'], $field['moduleSettings']), $matches, PREG_SET_ORDER );
    
    if(!empty($matches) && apply_filters('wal_ignore_shortcodes', true)){
       
        return false;
    }
   
    if($field['moduleSlug'] === 'image-icon' && in_array($field['uiType'], ['icon', 'media'])){
        $settings = (array)$field['moduleSettings'];
        $mode = $settings['image_type'];
        if($mode === 'icon' && $field['uiType'] !== 'icon'){
            return false;
        }
        if($mode === 'photo' && $field['uiType'] !== 'media'){
            return false;
        }
    }
    if($field['moduleSlug'] === 'adv-testimonials'){
        $settings = (array)$field['moduleSettings'];
        $mode = $settings['tetimonial_layout'];
        if($mode === 'slider' && 1 !== preg_match('~[0-9]~', $field['fieldUri'])){
            return false;
        }
    }
    if($field['moduleSlug'] === 'pp-iconlist'){
        $settings = (array)$field['moduleSettings'];
        $mode = $settings['list_type'];
        if($mode === 'number' && $field['uiType'] === 'icon'){
            return false;
        }
    }
    if($field['moduleSlug'] === 'team'){
        $settings = (array)$field['moduleSettings'];
        $mode = $settings['enable_custom_link'];
        if($mode === 'yes' && strpos($field['fieldUri'], 'name') !== false){
            return false;
        }
    }
    if($field['moduleSlug'] === 'pp-infolist'){
        $settings = (array)$field['moduleSettings'];
        preg_match('/\d+/', $field['fieldUri'], $matches);
        $instanceId = $matches[0];
        $linkMode = wal_getFieldDataFromUri('list_items ' . $instanceId . ' link_type', $field['moduleSettings']);
        if($linkMode === 'read_more' && $field['uiType'] === 'link'){
            return false;
        }
        if($linkMode === 'read_more' && strpos($field['fieldUri'], 'description') !== false){
            return false;
        }
        if($linkMode === 'title' && strpos($field['fieldSelector'], '.pp-list-item') !== false){
            return false;
        }
    }
    if($field['moduleSlug'] === 'info-list'){
        $settings = (array)$field['moduleSettings'];
        preg_match('/\d+/', $field['fieldUri'], $matches);
        $instanceId = $matches[0];
        $linkMode = wal_getFieldDataFromUri('add_list_item ' . $instanceId . ' list_item_link', $field['moduleSettings']);
        if($linkMode !== 'complete' && strpos($field['fieldSelector'], '.uabb-info-list-link') !== false){
            return false;
        }
        if($linkMode === 'list-title' && strpos($field['fieldUri'], 'list_item_title')){
            return false;
        }
    }

    

    return true;
});

add_filter('wal_matched_module_def_filter', function($moduleDef, $module){
    if($module->slug === 'adv-testimonials'){
        if($module->settings->tetimonial_layout === 'box'){
            $moduleDef = array(
                '.uabb-testimonial-author-description' => array(
                    'uri' => 'testimonial_description',
                    'uiType' => 'text'
                ),
                '.uabb-testimonial-author-name' => array(
                    'uri' => 'testimonial_author_no_slider',
                    'uiType' => 'text'
                ),
                '.uabb-testimonial-author-designation' => array(
                    'uri' => 'testimonial_designation_no_slider',
                    'uiType' => 'text'
                ),
                '.uabb-image-content' => array(
                    'uri' => 'photo_noslider',
                    'uiType' => 'media'
                ),
                '.uabb-icon i' => array(
                    'uri' => 'icon_noslider',
                    'uiType' => 'icon'
                )
            );
            return $moduleDef;
        }
    }
    
    
    return $moduleDef;


}, 10, 2);

