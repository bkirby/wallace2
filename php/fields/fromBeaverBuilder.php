<?php
namespace WallaceInline;


require_once('declared-modules.php');

use Rx\Observable;
use Rx\DisposableInterface;
use Rx\ObservableInterface;
use Rx\ObserverInterface;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use WallaceInline\ModuleDefinitions;

function fromBeaverBuilder($observable){
	if(!class_exists("FLBuilderModel") || is_admin()){
		return $observable;
	}
	$externalModulePostIdMap = getExternalModulePostIdMap();


		return $observable
		->concatMap(function($moduleOrRow){
			
			if($moduleOrRow->type === 'module'){
				$module = $moduleOrRow;
				return fromTextPreview($module)
				->concat(fromDefinitions($module));
			}
			if($moduleOrRow->type === 'row'){
				$row = $moduleOrRow;
				$field = array(
					'moduleId' => isset($row->template_node_id) ? $row->template_node_id : $row->node,
					'moduleSlug' => 'row',
					'moduleSettings' => $row->settings,
					'fieldUri' => 'bg_image',
					'fieldSelector' =>"[data-node='" . $row->node . "'] " . ' ' . '.fl-row-content-wrap',
					'uiType' => 'bgMedia',
				);
				return Observable::of($field);
			}
		})
	    ->map(function($field) use($externalModulePostIdMap){
	    	return determineDataSource($field, $externalModulePostIdMap);
		})
		->map(function($field){
	    	return determineRowId($field);
	    })
	    ->distinctKey(function ($field) {
	        return $field['fieldUri'] . $field['moduleId'];
	    })
	    ->filter(function($field){
			return userHasPermission($field) 
			&& fieldHasContent($field) 
			&& apply_filters('wal_pre_filter', $field)
			&& (function($field){if($field['uiType']==='bgMedia'){return true;}return !(apply_filters('wal_ignore_module', $field['module'])===true);})($field);
		})
		->map(function($field){
			unset($field['moduleSettings']);
			unset($field['module']);
			return $field;                                                                       
		});
	    
	}

	function getExternalModulePostIdMap(){
		$templatePosts = get_posts(['post_type' => 'fl-builder-template', 'numberposts' => -1]);
		$themerPosts = get_posts(['post_type' => 'fl-theme-layout', 'numberposts' => -1]);

		$globalNodeMap = array_reduce($templatePosts, function($acc, $templatePost){
			$globalNodes = \FlBuilderModel::get_layout_data('published', $templatePost->ID);
			foreach($globalNodes as $id => $node){
				$acc[$id] = $templatePost->ID;
			}
			return $acc;
		}, []);

		$themerNodeMap = array_reduce($themerPosts, function($acc, $themerPost){
			$themerNodes = \FlBuilderModel::get_layout_data('published', $themerPost->ID);
			foreach($themerNodes as $id => $node){
				$acc[$id] = $themerPost->ID;
			}
			return $acc;
		}, []);
		
		return array_merge($globalNodeMap, $themerNodeMap);
	}

	function getRowId($module, $postId){
		

		if($module->type === 'row'){
			return $module->node;
		}
		else{
			$parentNodeId = $module->parent;
			$data = \FlBuilderModel::get_layout_data('published', $postId);
			$parentNode = isset( $data[ $parentNodeId ] ) ? $data[ $parentNodeId ] : null;
			if($parentNode === null){
				return 'none';
			}
			else{
				return getRowId($parentNode, $postId);
			}
		}
		
	}

	function determineRowId($field){
		if($field['moduleSlug'] === 'row'){
			$field['rowId'] = $field['moduleId'];
			return $field;
		}
		$module = $field['module'];
		$postId = isset($field['dataSource']['postId']) ? $field['dataSource']['postId'] : -1;
		if($postId !== -1){
			$field['rowId'] = getRowId($module, $postId);
		} 
		return $field;
	}

	function determineDataSource($field, $idMap){


		//redo this
		
		
		$connections = (array)getObjectConnectionsFromUri($field['fieldUri'], $field['moduleSettings']); //should return key=>value like property=>fieldName

		$field['dataSource'] = array();
		global $wp;
		$url = home_url( $wp->request );

		//Post Content module from Beaver Themer template
		if($field['fieldUri'] === 'postContent'){
			$field['dataSource']['source'] = 'content';
			$field['dataSource']['postId'] = url_to_postid( $url );

			return $field;
		}

		//if no property then bb module
		if(!isset($connections['property'])){
			$field['dataSource']['source'] = 'bb_field';
			$field['dataSource']['postId'] = 
				isset($idMap[$field['moduleId']])
				? $idMap[$field['moduleId']] 
				: get_the_ID();

			// $field['dataSource']['postId'] = 
			// isset($field['module']->template_node_id)
			// 	? $idMap[$field['module']->template_node_id]
			// 	: get_the_ID();

			// if(isset($field['module']->template_node_id)){
			// 	$field['dataSource']['postId'] = $idMap[$field['module']->template_node_id];


			// }

	
			return $field;
		}

		if(in_array($connections['property'], ModuleDefinitions::getSupportedConnections())){

			if($connections['object'] !== 'post'){
				$field['dataSource']['source'] = 'unsupported';
				return $field;
			}
			if(in_array($connections['property'], array('pods_display', 'pods_photo'))){
				$field['dataSource']['pod'] = get_queried_object()->post_type;
			}
			
			$field['dataSource']['source'] = $connections['property'];
			$field['dataSource']['connection'] = $connections;
			$field['dataSource']['postId'] = url_to_postid( $url );
			
			return $field;
		}

		$field['dataSource']['source'] = 'unsupported';
		return $field;
		

	}

	function fromTextPreview($module){
		return Observable::create(function (ObserverInterface $observer) use ($module){
			$iterator  = new RecursiveArrayIterator($module->form);
			$recursive = new RecursiveIteratorIterator(
			    $iterator,
			    RecursiveIteratorIterator::SELF_FIRST
			);
			foreach($recursive as $key => $value){
				if(!isset($value['preview'])){
					continue;
				}
				if(!isset($value['preview']['type']) || !isset($value['preview']['selector'])){
					continue;
				}
				if($value['preview']['type'] === 'text'){
					$field = array(
						'moduleId' => isset($module->template_node_id) ? $module->template_node_id : $module->node,
						'moduleSlug' => $module->slug,
						'moduleSettings' => $module->settings,
						'fieldUri' => $key,
						'fieldSelector' => "[data-node='" . $module->node . "'] " . $value['preview']['selector'],
						'uiType' => 'text',
						'module' => $module,
						'locked' => isFieldLocked($module->node, $key),

					);
					// $observer->onNext($field);
					$observer->onNext($field);
				}
			}
			$observer->onCompleted();
		});
	}

	function fromDefinitions($module){
		return Observable::create(function (ObserverInterface $observer) use ($module){
			 //just register all declared fields for that module
			$moduleDefs = ModuleDefinitions::getBeaverModules();
			$matchedModuleDef = isset($moduleDefs[$module->slug]) ? $moduleDefs[$module->slug] : null;

			if($matchedModuleDef === null){
				$observer->onCompleted();
				return;
			}
			//filter matched module def
			$matchedModuleDef = apply_filters('wal_matched_module_def_filter', $matchedModuleDef, $module);
			array_map(function($selector, $fieldDef) use ($observer, $module){

			    //dynamic module case
				$pos = strpos($fieldDef['uri'], '{order}');
                if($pos !== false){
	                $preUri = substr($fieldDef['uri'], 0, $pos - 1 );
	                $subSettings = wal_getFieldDataFromUri($preUri, (array)$module->settings);
					foreach($subSettings as $key => $value){

						$pos2 = strpos($fieldDef['uri'], '{order2}');
						if($pos2 !== false){
							$preUri2 = substr($fieldDef['uri'], $pos+8, $pos2 - $pos - 9 );
							$subSettings2 = wal_getFieldDataFromUri($preUri2, (array)$subSettings[$key]);



							foreach($subSettings2 as $key2 => $value2){
								$field = array(
									'moduleId' => isset($module->template_node_id) ? $module->template_node_id : $module->node,
									'moduleSlug' => $module->slug,
									'moduleSettings' => $module->settings,
									'fieldUri' => str_replace(['{order}','{order2}'], [$key,$key2], $fieldDef['uri']),
									'fieldSelector' =>  "[data-node='" . $module->node . "'] " . str_replace(['{order}','{order2}'], ['{'.$key.'}','{'.$key2.'}'], $selector),
									'uiType' => $fieldDef['uiType'],
									'module' => $module,
									'locked' => isFieldLocked($module->node, str_replace(['{order}','{order2}'], [$key,$key2], $fieldDef['uri'])),
								);
								$observer->onNext($field);
							}
						}
						else{

							$field = array(
								'moduleId' => isset($module->template_node_id) ? $module->template_node_id : $module->node,
								'moduleSlug' => $module->slug,
								'moduleSettings' => $module->settings,
								'fieldUri' => str_replace('{order}', $key, $fieldDef['uri']),
								'fieldSelector' =>  "[data-node='" . $module->node . "'] " . str_replace('{order}', '{'.$key.'}', $selector),
								'uiType' => $fieldDef['uiType'],
								'module' => $module,
								'locked' => isFieldLocked($module->node, str_replace('{order}', $key, $fieldDef['uri'])),
							);
							if($field['uiType'] === 'link'){
								$field['link'] = wal_getFieldDataFromUri($field['fieldUri'], $field['moduleSettings']);
								$field['context'] = array_map(function($def) use($module, $key){
									return "[data-node='" . $module->node . "'] " . str_replace('{order}', '{'.$key.'}', $def);
								}, $fieldDef['context']);
							}
							if($field['uiType'] === 'icon'){
								$field['icon'] = wal_getFieldDataFromUri($field['fieldUri'], $field['moduleSettings']);
							}
							if(isset($fieldDef['altSelect'])){
								$field['altSelect'] = "[data-node='" . $module->node . "'] " . str_replace('{order}', '{'.$key.'}', $fieldDef['altSelect']);
							}
							if(isset($fieldDef['wrapper'])){
								$field['wrapper'] = $fieldDef['wrapper'];
							}

							$observer->onNext($field);
						}
					}
                }
                else{
                    //non dynamic module case
                    $field = array(
						'moduleId' => isset($module->template_node_id) ? $module->template_node_id : $module->node,
                        'moduleSlug' => $module->slug,
                        'moduleSettings' => $module->settings,
                        'fieldUri' => $fieldDef['uri'],
						'fieldSelector' => "[data-node='" . $module->node . "'] " . $selector,
						'uiType' => $fieldDef['uiType'],
						'module' => $module,
						'locked' => isFieldLocked($module->node, $fieldDef['uri']),
					);
					if($field['uiType'] === 'link'){
						$field['link'] = wal_getFieldDataFromUri($field['fieldUri'], $field['moduleSettings']);
						$field['context'] = array_map(function($def) use($module){
							return "[data-node='" . $module->node . "'] " . $def;
						}, $fieldDef['context']);
					}
					if($field['uiType'] === 'icon'){
						$field['icon'] = wal_getFieldDataFromUri($field['fieldUri'], $field['moduleSettings']);
					}
					if(isset($fieldDef['altSelect'])){
						$field['altSelect'] = "[data-node='" . $module->node . "'] " . $fieldDef['altSelect'];
					}
					if(isset($fieldDef['wrapper'])){
						$field['wrapper'] = $fieldDef['wrapper'];
					}
    				$observer->onNext($field);
                }
			}, array_keys($matchedModuleDef), $matchedModuleDef);

			$observer->onCompleted();
		});
	}

	function userHasPermission($field){
		if(!current_user_can('wal_full_access') && $field['locked']){ 
			return false;
		}
		return true;
	}

	function fieldHasContent($field){

		

		if($field['dataSource']['source'] === 'unsupported'){
			return false;
		}

		if($field['dataSource']['source'] === 'content' || $field['fieldUri'] === 'postContent'){
			return true;
		}

		$uri = $field['fieldUri'];
		$settings = (array) $field['moduleSettings'];
		$explode = explode(' ', $uri);


		 $fieldValue = array_reduce($explode, function($acc, $uriSegment) {
			$acc = (array) $acc;
		 	return $acc[$uriSegment];
		 }, $settings);



		if($fieldValue === null || $fieldValue === ''){
			return false;
		}

		if($uri === 'bg_image' && $settings['bg_type'] !== 'photo'){
			return false;
		}
		return true;
	}

	function getObjectConnectionsFromUri($uri, $moduleSettings){

		$moduleSettings = (array) $moduleSettings;

		$explode = explode(' ', $uri);
		if(count($explode) !== 1){
			$next = array_shift($explode);
			return getObjectConnectionsFromUri(implode(' ', $explode), $moduleSettings[$next]);
		}
		if(isset($moduleSettings['connections'])){
			$moduleSettings['connections'] = (array)$moduleSettings['connections'];
		}
		else{
			return false;
		}
		return isset($moduleSettings['connections'][$explode[0]]) 
		? $moduleSettings['connections'][$explode[0]] 
		: false;
	}

	function getFieldDataFromUri($uri, $settings){
		$settings = (array) $settings;
		$explode = explode(' ', $uri);
		if(count($explode) !== 1 ){
			$next = array_shift($explode);
			return getFieldDataFromUri(($next), $settings[$next]);
		}
		return $settings[$uri];
	}

	function isFieldLocked($moduleId, $fieldUri){
		$lockedOption = (array) json_decode(get_option('wal-locked'), true);;

		if(!isset($lockedOption)){
			return false;
		}
		else if(!isset($lockedOption[$moduleId])){
			return false;
		}
		else if(!isset($lockedOption[$moduleId][$fieldUri])){
			return false;
		}

		else{
			return $lockedOption[$moduleId][$fieldUri];

		}
	}

?>