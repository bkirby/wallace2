<?php
namespace WallaceInline;

class ModuleDefinitions{


public static function getSupportedConnections(){
	$connections = array('acf', 'acf_photo', 'title', 'content', 'pods_display', 'pods_photo');
		// 'postTitle' => array(
		// 	'field' => 'text',
		// 	'object' => 'post',
		// 	'property' => 'title'
		// ),
		// 'postContent' => array(
		// 	'field' => 'editor',
		// 	'object' => 'post',
		// 	'property' => 'content'
		// ),
		// 'pods_field' => array(
		// 	'property' => 'pods_display',
		// 	'field' => 'editor'
		// ),
		// 'acf' => array(
		// 	'property' => 'acf',
		// 	'field' => 'editor'
		// ),
		// 'acf' => array('acf_photo, editor'),
		// 'pods' => array('editor, photo'),
		// 'wp' => array('title, content')
		// 	'image' => 'acf_photo',
		// 	'text' => 'editor'
		// )
		// 'acf' => array(
		// 	'fieldTypes' => array(
		// 		'text' => array(
		// 			''
		// 		),
		// 		'image' => array(

		// 		)
		// 	)
		// )

	// );
	return $connections;
}


public static function getElementorModules(){
	$registeredModules = array(
		'text-editor' => array(
			'.elementor-text-editor' => array(
				'uri' => 'editor',
				'uiType' => 'text'
			)
		),
		'uael-infobox' => array(
			'.uael-infobox-title' => array(
				'uri' => 'infobox_title',
				'uiType' => 'text'
			),
			'.uael-infobox-text' => array(
				'uri' => 'infobox_description',
				'uiType' => 'text'
			)
		),
		'heading' => array(
			'.elementor-heading-title' => array(
				'uri' => 'title',
				'uiType' => 'text'
			)
		),
		'button' => array(
			'.elementor-button-text' => array(
				'uri' => 'text',
				'uiType' => 'text'
			),
			'.elementor-button-link' => array(
				'uri' => 'link url',
				'uiType' => 'link',
				'context' => array('.elementor-button-text')
			)
				
		),
		'call-to-action' => array(
			// '.elementor-cta__bg' => array(
			// 	'uri' => 'bg_image id',
			// 	'uiType' => 'media'
			// ),
			'.elementor-cta__title' => array(
				'uri' => 'title',
				'uiType' => 'text'
			),
			'.elementor-cta__description' => array(
				'uri' => 'description',
				'uiType' => 'text'
			),
			'.elementor-cta__button-wrapper a' => array(
				'uri' => 'button',
				'uiType' => 'text'
			)
		),
		'testimonial' => array(
			'.elementor-testimonial-content' => array(
				'uri' => 'testimonial_content',
				'uiType' => 'text'
			),
			'.elementor-testimonial-name' => array(
				'uri' => 'testimonial_name',
				'uiType' => 'text'
			),
			'.elementor-testimonial-job' => array(
				'uri' => 'testimonial_job',
				'uiType' => 'text'
			),
			'.elementor-testimonial-image' => array(
				'uri' => 'testimonial_image id',
				'uiType' => 'media'
			)
		),
		'accordion' => array(
			'.elementor-accordion-item {order} .elementor-tab-title a' => array(
				'uri' => 'tabs {order} tab_title',
				'uiType' => 'text'
			),
			'.elementor-accordion-item {order} .elementor-tab-content' => array(
				'uri' => 'tabs {order} tab_content',
				'uiType' => 'text'
			)
		),
		'price-table' => array(
			'.elementor-price-table__heading' => array(
				'uri' => 'heading',
				'uiType' => 'text',
			),
			'.elementor-price-table__subheading' => array(
				'uri' => 'sub_heading',
				'uiType' => 'text'
			),
			'.elementor-price-table__integer-part' => array(
				'uri' => 'price',
				'alturi' => 'integer',
				'uiType' => 'text'
			),
			'.elementor-price-table__fractional-part' => array(
				'uri' => 'price',
				'alturi' => 'fractional',
				'uiType' => 'text'
			),

			'.elementor-price-table__ribbon-inner' => array(
				'uri' => 'ribbon_title',
				'uiType' => 'text'
			),
			'.elementor-price-table__feature-inner {order} span' => array(
				'uri' => 'features_list {order} item_text',
				'uiType' => 'text'
			),
			'.elementor-price-table__button' => array(
				'uri' => 'button_text',
				'uiType' => 'text'
			),
			'.elementor-price-table__period' => array(
				'uri' => 'period',
				'uiType' => 'text'
			),
			'.elementor-price-table__additional_info' => array(
				'uri' => 'footer_additional_info',
				'uiType' => 'text'
			)
		),
		'price-list' => array(
			'.elementor-price-list-item {order} .elementor-price-list-title' => array(
				'uri' => 'price_list {order} title',
				'uiType' => 'text'
			),
			'.elementor-price-list-item {order} .elementor-price-list-description' => array(
				'uri' => 'price_list {order} item_description',
				'uiType' => 'text'
			),
			'.elementor-price-list-item {order} .elementor-price-list-price' => array(
				'uri' => 'price_list {order} price',
				'uiType' => 'text'
			)
		),
		'image-box' => array(
			'.elementor-image-box-img' => array(
				'uri' => 'image id',
				'uiType' => 'media'
			),
			'.elementor-image-box-title' => array(
				'uri' => 'title_text',
				'uiType' => 'text'
			),
			'.elementor-image-box-description' => array(
				'uri' => 'description_text',
				'uiType' => 'text'
			)
		),
		'icon-box' => array(
			'.elementor-icon-box-title' => array(
				'uri' => 'title_text',
				'uiType' => 'text'
			),
			'.elementor-icon-box-description' => array(
				'uri' => 'description_text',
				'uiType' => 'text'
			)
		),
		'image' => array(
			'.elementor-image' => array(
				'uri' => 'image id',
				'uiType' => 'media'
			)
		),
		'icon-list' => array(
			'.elementor-icon-list-item {order} .elementor-icon-list-text' => array(
				'uri' => 'icon_list {order} text',
				'uiType' => 'text'
			)
		),
		'premium-img-gallery' => array(
			'.premium-gallery-item {order}' => array(
				'uri' => 'premium_gallery_img_content {order} premium_gallery_img id',
				'uiType' => 'media'
			)
		)
	);
	return $registeredModules;
}


public static function getBeaverModules(){
	$registeredModules = array(


		


		'fl-post-content' => array(
			'.fl-module-content' => array(
				'uri' => 'postContent',
				'uiType' => 'text'
			)
		),
		'button' => array(
			'.fl-button-wrap a' => array(
				'uri' => 'link',
				'uiType' => 'link',
				'context' => array('.fl-button-text')
			),
			'.fl-button-text' => array(
				'uri' => 'text',
				'uiType' => 'text'
			)
		),
		'uabb-button' => array(
			'.uabb-button-text' => array(
				'uri' => 'text',
				'uiType' => 'text'
			),
			'a.uabb-button' => array(
				'uri' => 'link',
				'uiType' => 'link',
				'context' => array('.uabb-button-text')
			)
			
		),
		'uabb-call-to-action' => array(
			'.uabb-cta-title' => array(
				'uri' => 'title',
				'uiType' => 'text'
			),
			'.uabb-cta-text-content' => array(
				'uri' => 'text',
				'uiType' => 'text'
			),
			'.uabb-cta-button .uabb-button-text' => array(
				'uri' => 'btn_text',
				'uiType' => 'text'
			),
			'.uabb-cta-button a.uabb-button' => array(
				'uri' => 'btn_link',
				'uiType' => 'link',
				'context' => array('.uabb-cta-button .uabb-button-text')
			)
		),
		'list' => array(
			'.fl-list-item-heading-text {order}' => array(
				'uri' => 'list_items {order} heading',
				'uiType' => 'text'
			),
			'.fl-list-item-content-text {order}' => array(
				'uri' => 'list_items {order} content',
				'uiType' => 'text'
			)
		),

		'uabb-price-list' => array(
			'.uabb-price-list-title span {order}' => array(
				'uri' => 'add_price_list_item {order} price_list_item_title',
				'uiType' => 'text'
			),
			'.uabb-price-list-description {order}' => array(
				'uri' => 'add_price_list_item {order} price_list_item_description',
				'uiType' => 'text'
			),  
			'.uabb-pl-price-inner .uabb-price-list-price {order}' => array(
				'uri' => 'add_price_list_item {order} price',
				'uiType' => 'text'
			) 
		),

		'button-group' => array(
			'.fl-button-group-button .fl-button-text {order}' => array(
				'uri' => 'items {order} text',
				'uiType' => 'text'
			),
			'.fl-button-group-button a.fl-button {order}' => array(
				'uri' => 'items {order} link',
				'uiType' => 'link',
				'context' => array('.fl-button-group-button .fl-button-text {order}')
			),
			'.fl-button-group-button i.fl-button-icon {order}' => array(
				'uri' => 'items {order} icon',
				'uiType' => 'icon',
			)
		),
		
		'numbers' => array(
			'.fl-number-after-text' => array(
				'uri' => 'after_number_text',
				'uiType' => 'text'
			),
			'.fl-number-before-text' => array(
				'uri' => 'before_number_text',
				'uiType' => 'text'
			),
			'.fl-number-int' => array(
				'uri' => 'number',
				'uiType' => 'text'
			)

		),

		'tabs' => array(
			'.fl-tabs-label {order}' => array(
				'uri' => 'items {order} label',
				'uiType' => 'text'
			),
			'.fl-tabs-panel-content {order}' => array(
				'uri' => 'items {order} content',
				'uiType' => 'text'
			)
		),
		'photo' => array(
			'.fl-photo-content' => array(
				'uri' => 'photo',
				'uiType' => 'media'
			)
		),
		'uabb-photo' => array(
			'.uabb-photo-content' => array(
				'uri' => 'photo',
				'uiType' => 'media'
			)
		),
		'pp-image' => array(
			'.pp-photo-content-inner' => array(
				'uri' => 'photo',
				'uiType' => 'media'
			)
		),
		'icon' => array(
			'.fl-icon i' => array(
				'uri' => 'icon',
				'uiType' => 'icon'
			),
			'.fl-icon a' => array(
				'uri' => 'link',
				'uiType' => 'link',
				'context' => array('.fl-icon')
			)
		),
		'image-icon' => array(
			'.uabb-icon i' => array(
				'uri' => 'icon',
				'uiType' => 'icon'
			),
			'.uabb-image-content' => array(
				'uri' => 'photo',
				'uiType' => 'media'
			)

		),
		'heading' => array(
			'.fl-heading a' => array(
				'uri' => 'link',
				'uiType' => 'link',
				'context' => array('.fl-heading-text')
			),
			'.fl-heading-text' => array(
				'uri' => 'heading',
				'uiType' => 'text'
			)
		),
		'pp-testimonials' => array(
			'.pp-testimonials-heading' => array(
				'uri' => 'heading',
				'uiType' => 'text'
			),
			'.pp-testimonials-title {order}' => array(
				'uri' => 'testimonials {order} title',
				'uiType' => 'text'
			),
			'.pp-testimonials-subtitle {order}' => array(
				'uri' => 'testimonials {order} subtitle',
				'uiType' => 'text'
			),
			'.pp-testimonials-content {order}' => array(
				'uri' => 'testimonials {order} testimonial',
				'uiType' => 'text'
			),
			'.pp-testimonials-image {order}' => array(
				'uri' => 'testimonials {order} photo',
				'uiType' => 'media'
			)
		),
		'adv-testimonials' => array(
			'.uabb-testimonial-author-description {order}' => array(
				'uri' => 'testimonials {order} testimonial',
				'uiType' => 'text'
			),
			'.uabb-testimonial-author-name {order}' => array(
				'uri' => 'testimonials {order} testimonial_author',
				'uiType' => 'text'
			),
			'.uabb-testimonial-author-designation {order}' => array(
				'uri' => 'testimonials {order} testimonial_designation',
				'uiType' => 'text'
			),
			'.uabb-testimonial {order} .uabb-image-content' => array(
				'uri' => 'testimonials {order} photo',
				'uiType' => 'media'
			),
			'.uabb-testimonial {order} .uabb-icon i' => array(
				'uri' => 'testimonials {order} icon',
				'uiType' => 'icon'
			)
		),
		'pp-restaurant-menu' => array(
			'.pp-restaurant-menu-heading' => array(
				'uri' => 'menu_heading',
				'uiType' => 'text'
			),
			'.pp-restaurant-menu-item-title {order}' => array(
				'uri' => 'menu_items {order} menu_items_title',
				'uiType' => 'text'
			),
			'.pp-restaurant-menu-item-description {order}' => array(
				'uri' => 'menu_items {order} menu_item_description',
				'uiType' => 'text'
			),
			'.pp-restaurant-menu-item-price {order}' => array(
				'uri' => 'menu_items {order} menu_items_price',
				'uiType' => 'text'
			),
			'.pp-restaurant-menu-item {order} .pp-restaurant-menu-item-images' => array(
				'uri' => 'menu_items {order} menu_item_images',
				'uiType' => 'media'
			),
			'.pp-restaurant-menu-item {order} a' => array(
				'uri' => 'menu_items {order} menu_items_link',
				'uiType' => 'link',
				'context' => array('.pp-restaurant-menu-item {order} .pp-restaurant-menu-item-images')
			)
		),

		'uabb-heading' => array(
			'.uabb-heading-text' => array(
				'uri' => 'heading',
				'uiType' => 'text'
			),
			'.uabb-subheading' => array(
				'uri' => 'description',
				'uiType' => 'text'
			),
			'.uabb-heading a' => array(
				'uri' => 'link',
				'uiType' => 'link',
				'context' => array('.uabb-heading-text')
			),
			'.uabb-icon i' => array(
				'uri' => 'icon',
				'uiType' => 'icon',
			)
		),
		'testimonials' => array(
			'.fl-testimonial {order}' => array(
				'uri' => 'testimonials {order} testimonial',
				'uiType' => 'text'
			),
			'.fl-testimonials-heading' => array(
				'uri' => 'heading',
				'uiType' => 'text'
			)
		),
		'accordion' => array(
			'.fl-accordion-button-label {order}' => array(
				'uri' => 'items {order} label',
				'uiType' => 'text'
			),
			'.fl-accordion-content {order}' => array(
				'uri' => 'items {order} content',
				'uiType' => 'text'
			)
		),
		

		'pp-advanced-accordion' => array(
			'.pp-accordion-button-label {order}' => array(
				'uri' => 'items {order} label',
				'uiType' => 'text'
			),
			'.pp-accordion-content div {order}' => array(
				'uri' => 'items {order} content',
				'uiType' => 'text'
			),
			'.pp-accordion-icon {order}' => array(
				'uri' => 'items {order} accordion_font_icon',
				'uiType' => 'icon'
			)
		),

		'advanced-accordion' => array(
			'.uabb-adv-accordion-button-label {order}' => array(
				'uri' => 'acc_items {order} acc_title',
				'uiType' => 'text'
			),
			'.uabb-adv-accordion-content {order}' => array(
				'uri' => 'acc_items {order} ct_content',
				'uiType' => 'text'
			)
		),

		'list-icon' => array(
			'.uabb-list-icon-text-heading {order}' => array(
				'uri' => 'list_items {order} title',
				'uiType' => 'text'
			),
			'.uabb-icon i' => array(
				'uri' => 'icon',
				'uiType' => 'icon'
			)
		),

		'pp-infolist' => array(
			'.pp-infolist-title-text {order}' => array(
				'uri' => 'list_items {order} title',
				'uiType' => 'text'
			),
			'.pp-infolist-description {order}' => array(
				'uri' => 'list_items {order} description',
				'uiType' => 'text'
			),
			'.pp-infolist-icon-inner {order}' => array(
				'uri' => 'list_items {order} image_select',
				'uiType' => 'media'
			),
			'.pp-icon {order}' => array(
				'uri' => 'list_items {order} icon_select',
				'uiType' => 'icon'
			),
			'.pp-infolist-title {order} a' => array(
				'uri' => 'list_items {order} link',
				'uiType' => 'link',
				'context' => array('.pp-infolist-title-text {order}')
			),
			'.pp-list-item {order} a' => array(
				'uri' => 'list_items {order} link',
				'uiType' => 'link',
				'context' => array('.pp-infolist-title-text {order}', '.pp-infolist-description {order}', '.pp-icon {order}')
			)
		),

		'info-list' => array(
			'.uabb-info-list-title {order}' => array(
				'uri' => 'add_list_item {order} list_item_title',
				'uiType' => 'text'
			),
			'.uabb-info-list-description {order}' => array(
				'uri' => 'add_list_item {order} list_item_description',
				'uiType' => 'text'
			),
			'.uabb-info-list-item {order} .uabb-image-content' => array(
				'uri' => 'add_list_item {order} photo',
				'uiType' => 'media'
			),
			'.uabb-info-list-item {order} .uabb-icon i' => array(
				'uri' => 'add_list_item {order} icon',
				'uiType' => 'icon',
				'altSelect' => '.uabb-info-list-item {order} .uabb-info-list-icon'
			),
			'.uabb-info-list-item {order} .uabb-info-list-icon a' => array(
				'uri' => 'add_list_item {order} list_item_url',
				'uiType' => 'link',
				'context' => array('.uabb-info-list-item {order} .uabb-icon i')
			),
			'.uabb-info-list-item {order} .uabb-info-list-link' => array(
				'uri' => 'add_list_item {order} list_item_url',
				'uiType' => 'link',
				'context' => array(
						'.uabb-info-list-item {order} .uabb-icon i', 
						'.uabb-info-list-item {order} .uabb-image-content',
						'.uabb-info-list-description {order}',
						'.uabb-info-list-title {order}'
						)
			)

		),

		'pp-animated-headlines' => array(
			'.pp-headline-plain-text {0}' => array(
				'uri' => 'before_text',
				'uiType' => 'text'
			),
			'.pp-headline-dynamic-text' => array(
				'uri' => 'highlighted_text',
				'uiType' => 'text'
			),
			'.pp-headline-plain-text {1}' => array(
				'uri' => 'after_text',
				'uiType' => 'text'
			)
		),

		'pp-hover-cards' => array(
			'.pp-hover-card-title h3 {order}' => array(
				'uri' => 'card_content {order} title',
				'uiType' => 'text'
			),
			'.pp-hover-card-description {order}' => array(
				'uri' => 'card_content {order} hover_content',
				'uiType' => 'text'
			),
			'.pp-hover-card {order} .pp-more-link' => array(
				'uri' => 'card_content {order} button_text',
				'uiType' => 'text'
			),
			'.pp-hover-card {order} a.pp-more-link' => array(
				'uri' => 'card_content {order} button_link',
				'uiType' => 'link',
				'context' => array('.pp-hover-card {order} .pp-more-link')
			),
			'.pp-hover-card-container {order} .pp-more-link-container' => array(
				'uri' => 'card_content {order} button_link',
				'uiType' => 'link',
				'context' => array('.pp-hover-card-description {order}', '.pp-hover-card-title h3 {order}')
			)
		),
		'pp-logos-grid' => array(
			'.pp-logo-inner-wrap {order}' => array(
				'uri' => 'logos_grid {order} upload_logo_grid',
				'uiType' => 'media'
			),
			'.pp-logo-inner {order} .logo-title' => array(
				'uri' => 'logos_grid {order} upload_logo_title',
				'uiType' => 'text'
			), //todo: title is obscured by image, setup alt selector for img
			'.pp-logo {order} a' => array(
				'uri' => 'logos_grid {order} upload_logo_link',
				'uiType' => 'link',
				'context' => array('.pp-logo-inner-wrap {order}', '.pp-logo-inner {order} .logo-title')
			)
		), 

		'pp-timeline' => array(
			'.pp-timeline-title {order}' => array(
				'uri' => 'timeline {order} title',
				'uiType' => 'text'
			),
			'.pp-timeline-item {order} .pp-timeline-text p' => array(
				'uri' => 'timeline {order} content',
				'uiType' => 'text',
				'wrapper' => array('<p>', '</p>')
			),
			'.pp-timeline-item {order} .pp-timeline-button' => array(
				'uri' => 'timeline {order} button_text',
				'uiType' => 'text' //button styles get lost on save
			),
			'.pp-timeline-item {order} a.pp-timeline-button' => array(
				'uri' => 'timeline {order} button_link',
				'uiType' => 'link',
				'context' => array('.pp-timeline-item {order} .pp-timeline-button')
			),
			'.pp-timeline-item {order} .pp-icon' => array(
				'uri' => 'timeline {order} timeline_icon',
				'uiType' => 'icon'
			)
		),

		'pp-iconlist' => array(
			'.pp-list-item-text {order}' => array(
				'uri' => 'list_items {order}',
				'uiType' => 'text' //???
			),
			'.pp-icon-list-item .pp-list-item-icon' => array(
				'uri' => 'list_icon',
				'uiType' => 'icon'
			)
		),


		'pp-advanced-tabs' => array(
			'.pp-tabs-panel-content {order} [itemprop="text"]' => array(
				'uri' => 'items {order} content',
				'uiType' => 'text'
			),
			'.pp-tab-title {order}' => array(
				'uri' => 'items {order} label',
				'uiType' => 'text'
			),
			'.pp-tabs-panel-content {order} [itemprop="image"]' => array(
				'uri' => 'items {order} content_photo',
				'uiType' => 'media'
			),
			'.pp-tab-label-inner {order} .pp-tab-icon' => array(
				'uri' => 'items {order} tab_font_icon',
				'uiType' => 'icon'
			)
		),
		
		'advanced-tabs' => array(
			'.uabb-tab-acc-content {order}' => array(
				'uri' => 'items {order} ct_content',
				'uiType' => 'text'
			),
			'.uabb-tab-title {order}' => array(
				'uri' => 'items {order} label',
				'uiType' => 'text'
			),
			'.uabb-tabs-icon {order} i' => array(
				'uri' => 'items {order} tab_icon',
				'uiType' => 'icon'
			),
			'.section {order} .uabb-tab-acc-content' => array(
				'uri' => 'items {order} ct_photo',
				'uiType' => 'media'
			)
		),

		'tabs' => array(
			'.fl-tabs-panel-content {order}' => array(
				'uri' => 'items {order} content',
				'uiType' => 'text'
			),
			'.fl-tabs-label {order}' => array(
				'uri' => 'items {order} label',
				'uiType' => 'text'
			)
		),

		'pp-team' => array(
			'.pp-member-designation' => array(
				'uri' => 'member_designation',
				'uiType' => 'text'
			),
			'.pp-member-description' => array(
				'uri' => 'member_description',
				'uiType' => 'text'
			), 
			'.pp-member-name' => array(
				'uri' => 'member_name',
				'uiType' => 'text'
			),
			'.pp-member-image' => array(
				'uri' => 'member_image',
				'uiType' => 'media'
			)
		),

		'team' => array(
			'.uabb-team-desgn-text' => array(
				'uri' => 'designation',
				'uiType' => 'text'
			),
			'.uabb-team-desc-text' => array(
				'uri' => 'description',
				'uiType' => 'text'
			),
			'.uabb-team-name-text' => array(
				'uri' => 'name',
				'uiType' => 'text'
			),
			'.uabb-team-image' => array(
				'uri' => 'photo',
				'uiType' => 'media'
			)
		),
		'info-box' => array(
			'.uabb-infobox-title-prefix' => array(
				'uri' => 'heading_prefix',
				'uiType' => 'text'
			),
			'.uabb-infobox-title' => array(
				'uri' => 'title',
				'uiType' => 'text'
			),
			'.uabb-infobox-text p' => array(
				'uri' => 'text',
				'uiType' => 'text',
				'wrapper' => array('<p>', '</p>')
			),
			'.uabb-infobox-module-link' => array(
				'uri' => 'link',
				'uiType' => 'link',
				'context' => array('.uabb-infobox-title-prefix', '.uabb-infobox-title', '.uabb-infobox-text')
			),
			'.uabb-icon i' => array(
				'uri' => 'icon',
				'uiType' => 'icon'
			),
			'.uabb-button-text' => array(
				'uri' => 'btn_text',
				'uiType' => 'text'
			),
			'a.uabb-button' => array(
				'uri' => 'btn_link',
				'uiType' => 'link',
				'context' => array('.uabb-button-text')
			),
			'.uabb-infobox-cta-link' => array(
				'uri' => 'cta_text',
				'uiType' => 'text'
			),
			'a.uabb-infobox-cta-link' => array(
				'uri' => 'link',
				'uiType' => 'link',
				'context' => array('.uabb-infobox-cta-link')
			),
			'.uabb-image-content' => array(
				'uri' => 'photo',
				'uiType' => 'media'
			)
		),
		'gsr-soulsections' => array(
			'.ss-subsection {order} .ss-tagline-container' => array(
				'uri' => 'subsections {order} multisection_subsection_tagline',
				'uiType' => 'text'
			),
			'.ss-subsection {order} .ss-text-container' => array(
				'uri' => 'subsections {order} multisection_subsection_text',
				'uiType' => 'text'
			),
			'.ss-subsection {order} .ss-foreground-image-container' => array(
				'uri' => 'subsections {order} multisection_subsection_foreground_image',
				'uiType' => 'media'
			),
			'.ss-subsection .ss-foreground-image-container' => array(
				'uri' => 'singlesection_subsection_foreground_image',
				'uiType' => 'media'
			)
		),
		'pricing-table' => array(
			'.fl-pricing-table-column {order} .fl-pricing-table-title' => array(
				'uri' => 'pricing_columns {order} title',
				'uiType' => 'text'
			),
			'.fl-pricing-table-column {order} .fl-pricing-table-price' => array(
				'uri' => 'pricing_columns {order} price',
				'uiType' => 'text'
			),
			'.fl-pricing-table-column {order} .fl-pricing-table-features {order2} li' => array(
				'uri' => 'pricing_columns {order} features {order2}',
				'uiType' => 'text'
			),
			'.fl-pricing-table-column {order} .fl-button-text' => array(
				'uri' => 'pricing_columns {order} button_text',
				'uiType' => 'text'
			),
			'.fl-pricing-table-column {order} a.fl-button' => array(
				'uri' => 'pricing_columns {order} button_url',
				'uiType' => 'link',
				'context' => array('.fl-pricing-table-column {order} .fl-button-text')
			),
			// '.fl-pricing-table-column {order} .fl-pricing-table-duration' => array( //duration doesn't work do the html structure and a limitation in tinymce
			// 	'uri' => 'pricing_columns {order} duration', 
			// 	'uiType' => 'text'
			// )
			
		),
		'pp-pricing-table' => array(
			'.pp-pricing-table-col {order} .pp-pricing-featured-title' => array(
				'uri' => 'pricing_columns {order} hl_featured_title',
				'uiType' => 'text'
			),
			'.pp-pricing-table-col {order} .pp-pricing-table-title' => array(
				'uri' => 'pricing_columns {order} title',
				'uiType' => 'text'
			),
			'.pp-pricing-table-col {order} .pp-pricing-table-price' => array(
				'uri' => 'pricing_columns {order} price',
				'uiType' => 'text'
			),
			'.pp-pricing-table-col {order} .pp-pricing-table-features {order2} li' => array(
				'uri' => 'pricing_columns {order} features {order2}',
				'uiType' => 'text'
			),
			'.pp-pricing-table-col {order} .fl-button-text' => array(
				'uri' => 'pricing_columns {order} button_text',
				'uiType' => 'text'
			),
			'.pp-pricing-table-col {order} a.pp-pricing-package-button' => array(
				'uri' => 'pricing_columns {order} button_url',
				'uiType' => 'link',
				'context' => array('.pp-pricing-table-col {order} .fl-button-text')
			)
			),
		'pricing-box' => array(
			'.uabb-pricing-table-column {order} .uabb-pricing-table-title' => array(
				'uri' => 'pricing_columns {order} title',
				'uiType' => 'text'
			),
			'.uabb-pricing-table-column {order} .uabb-pricing-table-price' => array(
				'uri' => 'pricing_columns {order} price',
				'uiType' => 'text'
			),
			'.uabb-pricing-table-column {order} .uabb-pricing-table-features {order2} li' => array(
				'uri' => 'pricing_columns {order} features {order2}',
				'uiType' => 'text'
			),
			'.uabb-pricing-table-column {order} .uabb-button-text' => array(
				'uri' => 'pricing_columns {order} btn_text',
				'uiType' => 'text'
			),
			'.uabb-pricing-table-column {order} a.uabb-button' => array(
				'uri' => 'pricing_columns {order} btn_link',
				'uiType' => 'link',
				'context' => array('.uabb-pricing-table-column {order} .uabb-button-text')
			)
		)



		


		///needs testing

		// 'pp-pricing-table' => array(
		// 	'.pp-pricing-featured-title {order}' => array(
		// 		'uri' => 'pricing_columns {order} hl_featured_title',
		// 		'uiType' => 'text'
		// 	),
		// 	'.pp-pricing-table-title {order}' => array(
		// 		'uri' => 'pricing_columns {order} title',
		// 		'uiType' => 'text'
		// 	),
		// 	'.pp-pricing-package-button .fl-button-text {order}' => array(
		// 		'uri' => 'pricing_columns {order} button_text',
		// 		'uiType' => 'text'
		// 	)
		// )

		
	);
	return $registeredModules;
}
}


?>