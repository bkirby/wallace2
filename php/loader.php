<?php
namespace WallaceInline;

require_once 'update-config.php';
require_once 'iam/iam.php';
require_once 'vendor/autoload.php';
require_once 'fields/fields.php';
require_once 'endpoints/endpoints.php';

use React\EventLoop\Factory as EventLoopFactory;
use Rx\Scheduler;
use WallaceInline\FieldFactory;

class Loader{

	private static $FieldFactory = null;
	private static $eventLoop = null;
	private static $hasRun = false;

	public static function run(){
		add_action('wp', 'WallaceInline\Loader::init', 10, 0);
	}

	public static function init(){
		// // add_action( 'elementor/frontend/after_render', function ( \Elementor\Element_Base $element ) {
		// // 	$debug = 0;
			
		// // } );

		// add_action( 'elementor/editor/after_save', function( $post_id, $editor_data ) {
		// 	// Activity Log Plugin
		// 	$debug = 0;
		// });





		// return;

		self::setBaselineCapabilities();

		if(self::abort()){
			
			return;
		}

		self::excludeFromExternalMinification();
		self::setupPHPEventLoop();
		self::$FieldFactory = FieldFactory::getInstance();
		self::bootstrapClientApp();
		self::setupAdminMenuButtons();
		self::loadTranslations();
		self::loadClientAssets();
		
		self::$hasRun = true;
	}

	private static function abort(){
		if(self::$hasRun){
			return true;
		}
		if(apply_filters('wal_disable_editor', false, get_the_ID())){
			return true;
		}
		// if(!class_exists('FLBuilder') && !class_exists('Elementor') ){
		// 	return true;
		// }
		if(self::isUserAuthorized() === false){ //first pass user permission check, outside of IAM
			return true;
		}
		if (isset($_GET["fl_builder"])){
			add_action( 'admin_bar_menu', function($wp_admin_bar){
				$dot = ' <span class="fl-builder-admin-bar-status-dot" style="color:' . ( '#6bc373' ) . '; font-size:18px; line-height:1;">&bull;</span>';

				$wp_admin_bar->add_node( array(
					'href' => trailingslashit(get_permalink( get_the_ID() )) . '?wal_inline',
					'id'    => 'wal-inline-frontend-edit-link',
					'title' => '<span class="ab-icon"></span>' . esc_html__('Live Editor', 'wallace-inline') . $dot
				));
			}, 998);
			return true;
		}
		return false;
	}

	private static function excludeFromExternalMinification(){
		add_filter('rocket_exclude_js', function($fileList){
			$pathToDistJs = plugin_dir_url( __FILE__ ) . '../js/dist/main.js';
			array_push($fileList, $pathToDistJs);
			return $fileList; 
		});
	}

	private static function isUserAuthorized(){

		function hasOverride() {
			$user = wp_get_current_user()->data;
			$postId = get_the_ID();
			if(isset($postId) && isset($user->ID)){
				$permissionKey = 'user_ids';
				$uac = json_decode(get_option('wal-user-access'));
				$uacPost = isset($uac->$postId) ? $uac->$postId : null;
				$usersWithOverride = $uacPost !== null ? $uacPost->$permissionKey : null;
				// $usersWithOverride = isset((json_decode(get_option('wal-user-access'))->$postId)[$permissionKey]) ? json_decode(get_option('wal-user-access')->$postId->$permissionKey) : null;
				
				if($usersWithOverride !== null){
					forEach($usersWithOverride as $id){
						$id = (string) $id;
						if($id === $user->ID){
							return true;
						}
					}
				}
				return false;
			}
			else{
				return false;
			}
		}
		if(current_user_can('wal_limited_access') || current_user_can('wal_full_access') || hasOverride()){
			return true;
		}
		return false;
	}

	private static function setBaselineCapabilities(){
		$adminRole = get_role( 'administrator' );
		if(isset($adminRole)){
			$adminRole->add_cap( 'wal_full_access' );
		}

		$editorRole = get_role( 'editor' );
		$uac = get_option('wal-user-access');
		if(get_option('wal-user-access') === false && isset($editorRole)){
			$editorRole->add_cap( 'wal_limited_access' );
		}
	}

	private static function setupPHPEventLoop(){
		$loop = EventLoopFactory::create();

		Scheduler::setDefaultFactory(function() use($loop){
		    return new Scheduler\EventLoopScheduler($loop);
		});

	    add_action('wp_footer', function() use($loop){
	    	$loop->run();
	    }, 9, 0);
	}

	private static function bootstrapClientApp(){
		add_action('wp_footer', function(){
			echo '<div id="wallace-inline"></div>';
		}, 8, 0);


		add_action('wp_footer', function(){
			$editable = self::$FieldFactory->currentPageHasEditableFields();
			
			// if(self::$FieldFactory->currentPageHasEditableFields() === false){
			// 	// echo '<script>
			// 	// 	document.getElementById("wp-admin-bar-wal-inline-frontend-edit-link").style.display = "none"
			// 	// </script>';
			// 	return;
			// }
			$fields = self::$FieldFactory->getFields();
			wp_localize_script('wallace-inline', 'wallaceInline', array(
				'editableFields' => self::$FieldFactory->getFields(),
				'connectionInfo' => array(
					'skinUrl' => plugins_url('/wallace-inline/tinymceskinfix'),
					'walRestURL' => esc_url_raw( rest_url('wallaceinline/v2/') ),
					'wpRestURL' => esc_url_raw( rest_url('wp/v2/') ),
					'nonce' => wp_create_nonce('wp_rest'),
					'postId' => get_the_ID(),
					'userHasFullAccess' => current_user_can( 'wal_full_access' )
				),
				'uac' => self::getUAC(),
				'openImmediately' => isset($_GET["wal_inline"]),
				'iconSelectorHTML' => class_exists('\FLBuilderUISettingsForms') ? \FLBuilderUISettingsForms::render_icon_selector() : '0',
				'strings' => self::getTranslations(),

			));
		}, 11, 0);

		add_action( 'wp_after_admin_bar_render', function(){
			// if(self::$FieldFactory->currentPageHasEditableFields() === false){
			// 	return;
			// }
			echo '<script>
					document.getElementById("wp-admin-bar-wal-inline-frontend-edit-link").onclick=function(){
						document.dispatchEvent(new CustomEvent("walInlineAdminButtonClicked"))
					}
				</script>';
		}, 10, 0);
	}
	private static function setupAdminMenuButtons(){
		add_action( 'admin_bar_menu', function($wp_admin_bar){
			$isAdmin = is_admin();
			$hasFields = self::$FieldFactory->currentPageHasEditableFields();
			if(is_admin()){
				return;
			}

			$dot = ' <span class="fl-builder-admin-bar-status-dot" style="color:#6bc373; font-size:18px; line-height:1;">&bull;</span>';
			$link = '';

			if(isset($_GET["fl_builder"])){ //if builder active
				$link = trailingslashit(get_permalink( get_the_ID() )) . '?wal_inline';
			}

			$wp_admin_bar->add_node( array(
				'href' => $link,
				'id'    => 'wal-inline-frontend-edit-link',
				'title' => '<span class="ab-icon"></span>' . __('Live Editor', 'wallace-inline') . $dot
			));
		}, 998, 1);
	}

	private static function loadTranslations() {
		if ( function_exists( 'get_user_locale' ) ) {
			$locale = apply_filters( 'plugin_locale', get_user_locale(), 'fl-builder' );
		} else {
			$locale = apply_filters( 'plugin_locale', get_locale(), 'fl-builder' );
		}

		$mofile_global = trailingslashit( WP_LANG_DIR ) . 'plugins/wallace-inline-' . $locale . '.mo';
		$mofile_local  = trailingslashit( dirname(WAL_PLUGIN_FILE_PATH) ) . 'languages/' . $locale . '.mo';

		if ( file_exists( $mofile_global ) ) {
			load_textdomain( 'wallace-inline', $mofile_global );
		} elseif ( file_exists( $mofile_local ) ) {
			load_textdomain( 'wallace-inline', $mofile_local );
		}
	}

	private static function loadClientAssets(){

		

		add_action('wp_footer', function(){



			$editable = self::$FieldFactory->currentPageHasEditableFields();
			// if(self::$FieldFactory->currentPageHasEditableFields() === false){
			// 	return;
			// }
			

			// wp_enqueue_script( 'wal_tinymce_js', plugin_dir_url( __FILE__ ) . '../js-libs/tinymce/tinymce.min.js', array( 'jquery' ), false, true );
			wp_deregister_script( 'jquery-bxslider' );
			wp_register_script( 'jquery-bxslider', plugin_dir_url( __FILE__ ) . '../js-libs/jquery.bxslider.js', array( 'jquery-easing', 'jquery-fitvids'), false, true );
			echo '<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">';
			// wp_enqueue_script( 'tinymce_js', includes_url( 'js/tinymce/' ) . 'wp-tinymce.php', array( 'jquery' ), false, true );

			// wp_enqueue_script( 'wallace-inline', plugin_dir_url( __FILE__ ) . '../js/dist/main.js', array('tinymce_js'), '1.0', true);
			wp_enqueue_script( 'wallace-inline', 'http://localhost:8080/main.js', array(), '1.0', true);

			if(class_exists('FLBuilder')){
				wp_enqueue_style( 'bb-icon-selector', plugins_url('/bb-plugin/css/fl-icon-selector.css'), array(), 999);
				wp_enqueue_style( 'bb-lightbox', plugins_url('/bb-plugin/css/fl-lightbox.css'), array(), 999);
			}
			wp_enqueue_style( 'wallace-inline', plugin_dir_url( __FILE__ ) . '../styles.css', array(), 999);
			wp_enqueue_style( 'wal-tiny-fix', plugins_url('/wallace-inline/tinymceskinfix/skin.min.css'), array(), 999);
			wp_enqueue_style( 'wal-tiny-fix2', plugins_url('/wallace-inline/tinymceskinfix/content.inline.min.css'), array(), 999);

		}, 10, 0);

		add_action('wp_enqueue_scripts', function(){

			wp_enqueue_media();

		}, 10, 0);

		add_action('wp_footer', function(){
			// self::$FieldFactory->currentPageHasEditableFields() === false
			$hasEditable = self::$FieldFactory->currentPageHasEditableFields();

				echo '<div id="svg-storage" style="position:absolute; z-index: -99; bottom: 0; display: none">';
		
				echo '<svg id="wal-lock-open"  xmlns="http://www.w3.org/2000/svg">
			<path d="M0 0h24v24H0z" fill="none"/>
			<path id="wal-lock-open-inner" d="M12 17c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm6-9h-1V6c0-2.76-2.24-5-5-5S7 3.24 7 6h1.9c0-1.71 1.39-3.1 3.1-3.1 1.71 0 3.1 1.39 3.1 3.1v2H6c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V10c0-1.1-.9-2-2-2zm0 12H6V10h12v10z"/>
		</svg>';
		
				echo '<svg id="wal-lock-closed" xmlns="http://www.w3.org/2000/svg">
			<path d="M0 0h24v24H0z" fill="none"/>
			<path d="M18 8h-1V6c0-2.76-2.24-5-5-5S7 3.24 7 6v2H6c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V10c0-1.1-.9-2-2-2zm-6 9c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2zm3.1-9H8.9V6c0-1.71 1.39-3.1 3.1-3.1 1.71 0 3.1 1.39 3.1 3.1v2z"/>
		</svg>';
		
			echo '<svg id="wal-bg-select" xmlns="http://www.w3.org/2000/svg">
			<path fill="none" d="M24 24H0V0h24v24z"/>
			<path d="M21 15h2v2h-2v-2zm0-4h2v2h-2v-2zm2 8h-2v2c1 0 2-1 2-2zM13 3h2v2h-2V3zm8 4h2v2h-2V7zm0-4v2h2c0-1-1-2-2-2zM1 7h2v2H1V7zm16-4h2v2h-2V3zm0 16h2v2h-2v-2zM3 3C2 3 1 4 1 5h2V3zm6 0h2v2H9V3zM5 3h2v2H5V3zm-4 8v8c0 1.1.9 2 2 2h12V11H1zm2 8l2.5-3.21 1.79 2.15 2.5-3.22L13 19H3z"/>
		</svg>
		';
		
				echo '
		<svg id="wal-spinner" stroke="#fff">
			<g fill="none" fill-rule="evenodd">
				<g transform="translate(1 1)" stroke-width="2">
					<circle stroke-opacity=".5" cx="11" cy="11" r="11"/>
					<path d="M22 11c0-9.94-8.06-11-11-11">
						<animateTransform
							attributeName="transform"
							type="rotate"
							from="0 11 11"
							to="360 11 11"
							dur="1s"
							repeatCount="indefinite"/>
					</path>
				</g>
			</g>
		</svg>';
		
			echo '
		<svg id="wal-more" stroke="#fff">
			<path d="M0 0h24v24H0z" fill="none"/>
			<path d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/>
		</svg>';

		echo '</div>';
		}, 7, 0);
	}

	private static function getUAC(){
		global $post;
		$post_id = $post->ID;
		$ids_key = 'user_ids';
		$user_access_overrides = isset(json_decode(get_option('wal-user-access'))->$post_id->$ids_key) ? json_decode(get_option('wal-user-access'))->$post_id->$ids_key : null;
		$limited_access_roles = [];
		$full_access_roles = [];
		$available_roles = [];
		foreach(apply_filters( 'editable_roles', wp_roles()->roles ) as $registeredRoleName => $registeredRole){
			$roleData = array(
				'label' => '',
				'value' => ''
			);

			$roleData['label'] = $registeredRole['name'];
			$roleData['value'] = $registeredRoleName;
			array_push($available_roles, $roleData);

			if(get_role($registeredRoleName)->has_cap('wal_limited_access')){
				array_push($limited_access_roles, $roleData);
			}
			else if(get_role($registeredRoleName)->has_cap('wal_full_access')){
				array_push($full_access_roles, $roleData);
			}
			else{
				//no access
			}
		}
		// print '<script>console.log(' . json_encode($limited_access_roles) . ');</script>';


		$users = [];
		if(isset($user_access_overrides)){
			foreach($user_access_overrides as $id){
				$user = get_user_by('id', $id)->data;
				$userData = array(
					'name' => '',
					'id' => ''
				);

				$userData['name'] = $user->display_name;
				$userData['id'] = (string) $user->ID;
				$userData['label'] = $userData['name'] . ' (' . $userData['id'] . ')';
				$userData['value'] = $userData['label'];

				array_push($users, $userData);
			}
		}
		return array(
			'userOverrides' => $users,
			'roles' => $available_roles,
			'limitedRoles' => $limited_access_roles,
			'fullRoles' => $full_access_roles
		);
	}

	private static function getTranslations(){

	
		return array(
			'editorLabel' => __('Live Editor', 'wallace-inline'),
			'editText' => __('Edit', 'wallace-inline'),
			'unlockTooltip' => __('Allow editing by Limited Capability users', 'wallace-inline'),
			'lockTooltip' => __('Restrict editing to Full Capability users only', 'wallace-inline'),
			'bgTooltip' => __('Change background image', 'wallace-inline'),
			'doneText' => __('Done', 'wallace-inline'),
			'discardText' => __('Discard', 'wallace-inline'),
			'cancelText' => __('Cancel', 'wallace-inline'),
			'publishText' => __('Publish', 'wallace-inline'),
			'successStatusText' => __('Success!', 'wallace-inline'),
			'publishingStatusText' => __('Publishing...', 'wallace-inline'),
			'imageSelectPrompt' => __('Select or Upload Media', 'wallace-inline'),
			'imageChooseButton' => __('Use this image', 'wallace-inline'),
			//frontend settings page
			'settingsLabel' => __('Settings', 'wallace-inline'),
			'roleLabel' => __('Role Management', 'wallace-inline'),
			'fullCapabilityLabel' => __('Full Capability', 'wallace-inline'),
			'fullCapabilityDescription' => __("Users with these roles have full plugin capabilites including managing other users' permissions. Typically these are site admins", 'wallace-inline'),
			'limitedCapabilityLabel' => __('Limited Capability', 'wallace-inline'),
			'limitedCapabilityDescription' => __("Users with these roles have their editing access limited by Full Capability users and can't edit other users' permission", 'wallace-inline'),
			'singlePageLabel' => __('Single-Page Limited Capability', 'wallace-inline'),
			'singlePageDescription' => __("Give individual users Limited Capability to edit only this page", 'wallace-inline'),
			'selectLabel' => __('Select...', 'wallace-inline'),
			'typeLabel' => __('Type to search', 'wallace-inline'),
			'editLink' => __('Edit Link', 'wallace-inline'),
			'pasteLink' => __('Paste New Link Here', 'wallace-inline'),
			'advancedSettings' => __('Advanced Settings', 'wallace-inline')
		);
	}
}
?>